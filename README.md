# ARCWP-Functionality #

Autism Research Centre (ARC) Functionality Plugin

### What is this repository for? ###

* Adds Staff custom post type
* Adds Project custom post type
* Adds Publication custom post type
* Adds Staff taxonomies: Roles, Keywords, Staff Affiliations
* Adds Project taxonomies: Parent Programme
* Adds Publication taxonomies: Parent Programme (shared),Publishers, Keyword(shared),Publication Journals

### How do I get set up? ###

* Dependencies: StudioPress genesis child theme 
* Install to plugins directory and activate via Dashboard > plugins

### Who do I talk to? ###

* Dave Tasker (dt216)
* Dave Connor (dc677)
