<?php
/**
 * User: am867
 * Date: 08/08/2019
 */

namespace cscs_ns\src;

class CSCS_News_Widget extends \WP_Widget {

	// Main constructor
	public function __construct() {
		parent::__construct(
			'cscs_single_news_widget',
			__( 'CSCS News Highlight Widget', 'text_domain' ),
			array(
				'customize_selective_refresh' => true,
			)
		);
	}

	// The widget form (for the backend )
	public function form( $instance ) {
		// Set widget defaults
		$defaults = array(
			'title'    => 'Latest news',
		);

		// Parse current settings with defaults
		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

		<?php // Widget Title ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<?php
	}

	// Update widget settings
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		return $instance;
	}

	// Display the widget
	public function widget( $args, $instance ) {
		extract( $args );

		// Check the widget options
		$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : 'Latest News';
		// WordPress core before_widget hook (always include )
		echo $before_widget;

		// Display the widget
		echo '<div class="widget-text wp_widget_plugin_box">';

		// Display widget title if defined
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		$args = array(
			'post_type' => 'post',
            'posts_per_page' => 1
		);

		$custom_posts = new \WP_Query($args);
		// The Loop
		if ( $custom_posts->have_posts() ) {
			echo '<div class="news-home-block">';
			while ( $custom_posts->have_posts() ) {
				$custom_posts->the_post();
				echo '<div class="news-item">';

				

				if(has_post_thumbnail()){
					$image_mobile = wp_get_attachment_image_src( get_post_thumbnail_id(  get_the_ID() ), 'homepage-thumb' )[0];
					echo '<div class="news-image">';
					echo '<a href="'.get_permalink().'">';
					echo '<img src="'.$image_mobile.'" class="project-image-img" />';
					echo '</a>';
					echo '</div>';
				}

				echo '<div class="news-summary">';
				echo '<a href="'.get_permalink().'">';
				echo '<h2>' . get_the_title() . '</h2>';
				echo '</a>';

				//$text = get_field('project_content');
                $text = get_the_excerpt();
				// if ( '' != $text ) {
				// 	$text = strip_shortcodes( $text );
				// 	$text = apply_filters('the_content', $text);
				// 	$text = str_replace(']]&gt;', ']]&gt;', $text);
				// 	$excerpt_length = 20; // 20 words
				// 	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
				// 	$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
				// }
				$text = apply_filters('the_excerpt', $text);

				echo '<p>'.$text.'</p>';
				echo '<a href="'.get_permalink().'" class="button small">Read more</a>';
				echo '</div>';
                
				echo '</div>';
			}
			echo '</div>';
		} else {
			// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();

		echo '</div>';

		// WordPress core after_widget hook (always include )
		echo $after_widget;
	}

}