<?php
/**
 * Author:      Dave Tasker
 * Author URI:  https://cscs.medschl.cam.ac.uk
 * Version
**/

namespace cscs_ns\src;


class Site_Options
{
    public function __construct( $args = array() ) {

        add_action( 'init', array($this,'dt216_acf_options_page') );

        add_filter( 'genesis_footer_creds_text', array($this, 'dt216_footer_creds_text' ));

        add_action('acf/init', array($this,'acf_option_metaboxes'));

    }

    public function dt216_acf_options_page()

    {

        if (function_exists('acf_add_options_page')) {

            acf_add_options_page(array(
                'page_title' => 'Theme General Settings',
                'menu_title' => 'Theme Settings',
                'menu_slug' => 'theme-general-settings',
                'capability' => 'edit_posts',
                'redirect' => true,
            ));


            acf_add_options_sub_page(array(
                'page_title' => 'Theme Footer Settings',
                'menu_title' => 'Footer',
                'parent_slug' => 'theme-general-settings',
            ));
        }
    }

    public function dt216_footer_creds_text() {

        echo '<div class="creds"><p>';

        echo 'Copyright &copy; ';

        echo(date('Y') . '&nbsp;');

        $credits = wp_kses(get_option('options_dt216_footer_credits'), array(
            'a' => array(
                'href' => array(),
                'title' => array()
            ),
            'br' => array(),
            'em' => array(),
            'strong' => array(),
        ));

        echo  $credits;

        //echo '&nbsp;&sdot;&nbsp; <a href="https://cscs.medschl.cam.ac.uk/webservices/"">Hosted by CSCS</a></p></div>';
    }

    /*
     * Create the ACF metaboxes
     */

    public function acf_option_metaboxes() {

                 acf_add_local_field_group(array(
                'key' => 'group_5554aa547d140',
                'title' => 'Footer Credits',
                'fields' => array(
                    array(
                        'key' => 'field_5554aa60ac161',
                        'label' => 'dt216_footer_credits',
                        'name' => 'dt216_footer_credits',
                        'type' => 'wysiwyg',
                        'instructions' => 'Enter your text for the theme footer credit',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 'My Site · send comments to <a href="mailto:xyz@mail.com">xyz@mail.com</a>',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'media_upload' => 1,
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'acf-options-footer',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
            ));

    }

}