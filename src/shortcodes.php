<?php
/**
 * Staff Listing Shortcodes
 *
 * This file creates shortcodes for listing staff CPT
 *
 * @package      Staff CPT and shortcode
 * @since        0.1.7
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
 */

function staff_list($atts) {

	$content = null;
    extract(shortcode_atts(array(
        "alphabet" => '',
        "affiliation" => '',
        "role" => '',
        "list" => '',
        "image" => '',
	    'clear' =>'',
    ), $atts));
    if ($alphabet == 'a-to-g') { $range = 'A-to-G'; }
    elseif ($alphabet == 'h-to-m') { $range = 'H-to-M'; }
    elseif ($alphabet == 'n-to-s') { $range = 'N-to-S'; }
    elseif ($alphabet == 't-to-z') { $range = 'T-to-Z'; }
    if ($alphabet) { $meta_alphabet = array('key' => 'alphabet_group','value' => $range,'compare' => '='); } //Alphabet field
    if ($affiliation) { $tax_affiliation = array('taxonomy' => 'staff_affiliation','field' => 'slug','terms' => $affiliation); }
    if ($role) { $tax_role = array('taxonomy' => 'staff_role','field' => 'slug','terms' => $role); }

    global $post;
    $posts = get_posts(array(
        'numberposts' => -1,
        'post_type' => 'staff',
        'tax_query' => array (
            'affiliation'   => $tax_affiliation,
            'role'          => $tax_role,
            'group'         => $tax_group
//        ),
//        'meta_query' => array (
//            $meta_archived,$meta_alphabet,$meta_theme,$meta_collaboration
        ),
        'meta_key' => 'staff_surname', //surname field
        'orderby' => array(
            'role' => 'ASC',
            'staff_surname' => 'DESC'
        ),
        //'order' => 'ASC'
    ));

    /* TODO: rewrite this all. */
    if($posts) {
        $staffOutput = '';
        if (!$list) {
                    $staffOutput .= '<div class="staff-grid">';
        } else{ $staffOutput .= '<ul>'; }
        foreach($posts as $post) {
            $permalink = get_permalink(get_page_by_title(get_the_title()));
            $jobtitles = get_the_terms( $post->ID, 'staff_title' );
            $jobs = array();
            foreach ($jobtitles as $job) {
                $jobs[] = $job->name;
            }
            $jobs = join(", ", $jobs);

            $honorific = get_post_meta( get_the_ID(), 'staff_honorific', true );
            if(strlen($honorific) > 0) {
                $honorific .= "&nbsp;";
            }
            $jobtitle = get_post_meta( get_the_ID(), 'staff_job_title', true );
            $firstname = get_post_meta( get_the_ID(), 'staff_first_name', true );
            $surname = get_post_meta( get_the_ID(), 'staff_surname', true );

            if ($list) {
                 $staffOutput .= '<li><a href="' . $permalink . '" class="">' . get_post_meta( get_the_ID(), 'staff_forename', true ) . '&nbsp;' . get_post_meta( get_the_ID(), 'staff_surname', true ).'</a></li>';
            } else if($image) {
            	$staffOutput .= '<div class="staff-item">';
                $staffMugshot = get_post_meta(get_the_ID(), 'staff_photo', true);
                $imageURL = wp_get_attachment_image_src($staffMugshot, 'staff-portrait');
                //$imageAlt = get_post_meta($image, '_wp_attachment_image_alt', true);
                // Updated to add additional photo source
                if( isset($imageURL[0]) && strlen($imageURL[0]) >= 5) {
                    $image_mobile = $imageURL[0];
                } else if(has_post_thumbnail()) {
                    $image_mobile = wp_get_attachment_image_src( get_post_thumbnail_id(  get_the_ID() ), 'staff-portrait' )[0];
                } else {
                    $image_mobile = plugins_url('images/generic-person-silhouette.jpg',dirname(__FILE__) ) ;
                }
    
                $staffOutput .= '<a href="' . $permalink . '">' .'<img src="'.$image_mobile.'" class="staff-image" />'.'</a>';

                $staffOutput .= '<div class="staff-info"><div class="staff-name">';
                
                $staffOutput .= '<a href="' . $permalink . '">' . $honorific . $firstname . '&nbsp;' . $surname . '</a>';
                $staffOutput .= '</div>';
                $staffOutput .= '<div class="job-title">'.$jobtitle.'</div>';

                $staffOutput .= '</div></div>';


            } else {
                $staffOutput .= '<div class="facet-row">';
                $staffOutput .= '<div class ="one-half first facet"><strong>';
                $staffOutput .= '<a href="' . $permalink . '">' . $honorific . $firstname . '&nbsp;' . $surname . '</a>';
                $staffOutput .= '</strong></div>';
	            $staffOutput .= '<div class ="one-half facet">&nbsp;'. $jobtitle .'</div>';
	        
                $staffOutput .= '</div>';
            }
        }

        if (!$list) {
            $staffOutput .= '</div>';
            // Implement the shortcode clear parameter
	        if ($clear){
		        $staffOutput .= ' <div class="clearfix"></div>';
	        }
        } else {
            $staffOutput .= '</ul>';
        }
    }
    if(isset($staffOutput)){
        wp_reset_postdata();
        return $staffOutput;
    }
}
add_shortcode("stafflist", "staff_list");


/**
 * Staff Listing Shortcodes
 *
 * This file creates shortcodes for listing staff CPT
 *
 * @package      Staff CPT and shortcode
 * @since        0.1.7
 * @link
 * @author       Dave Connor
 */

function cscs_staff_category( $atts ) {
	
	if ( ! empty( $atts['category'] ) ) {
		$category = $atts['category'];
	} elseif ( ! empty( $atts[0] ) ) {
		$category = $atts[0];
	} else {
		return '<!-- Missing Getty Source ID -->'.print_r($atts, true);
	}

    $term = get_term_by('slug', $category, 'staff_role');
    $value = $term->name;

    if(strlen($value) <= 0) {
        $value = "People";
    }
   
	return $value;	
}

add_shortcode('staffcategory', 'cscs_staff_category');

/**
 * Link button shortcode
 *
 * This creates a styled button in the content
 *
 */

function cscs_button_link( $atts, $content ) {
	$a = shortcode_atts( array(
		'class' => '',
        'style' => '',
        'category' => 'button',
        'label' => $content,
        'value' => '',
        'link' => ''
	), $atts );

	return '<a class="button ' . esc_attr($a['class']) . '" style="' . esc_attr($a['style']) . '" data-category="' . esc_attr($a['category']) . '" data-label="' . esc_attr($a['label']) . '" data-ga-value="' . esc_attr($a['value']) . '" href="' . esc_attr($a['link']) . '">' . $content . '</a>';
}

add_shortcode('button', 'cscs_button_link');

/**
 * Logo box layout
 * 
 * Creates a box to show logos set out using flexbox
 */

function cscs_logobox_single( $atts, $content ) {
    $a = shortcode_atts( array(
        'background' => '',
        'link' => '',
        'title' => '',
        'textcolour' => '#002691'
	), $atts );
    
    $output = '';
    $output .= "<div class='logo logo-single' ";
    if( '' !== $a['background'] ) {
        $output .= "style='background-color: {$a['background']}; color: {$a['textcolour']};'";    
    }
    $output .= ">";
    $output .= "<div><a href='{$a['link']}' title='{$a['title']}'>";
    $output .= $content;
    $output .= "</a></div>";
    $output .= "<div class='spacer'></div>";
    $output .= "<div><a href='{$a['link']}' style='color: {$a['textcolour']};'>{$a['title']}</a></div>";
    $output .= "</div>";
    
    return $output;
}

add_shortcode( 'logo', 'cscs_logobox_single' );

add_filter( 'no_texturize_shortcodes', 'ignore_logo' );

function ignore_logo( $list ) {
  $list[] = 'logo';
  return $list;
}

/**
 * Logo box wrapper
 * 
 * creates flexbox wrapper for logos
 */

function cscs_logobox_wrap ( $atts, $content ) {
    $a = shortcode_atts( array(	), $atts );
    
    $output = '';
    $output .= "<div class='logowrap'>";
    $output .= do_shortcode( strip_tags( $content, '<a><img><div>') );
    $output .= "</div>";
    
    return $output;
}

add_shortcode( 'logowrap', 'cscs_logobox_wrap' );

add_filter( 'no_texturize_shortcodes', 'ignore_logowrap' );

function ignore_logowrap( $list ) {
  $list[] = 'logowrap';
  return $list;
}