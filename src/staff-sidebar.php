<?php
/**
 * Build the Right Sidebar
 *
 * @package      Staff CPT and shortcode
 * @since        0.1.4
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
 */

add_action ( 'genesis_sidebar', 'profile_primary_sidebar', 1 );
function profile_primary_sidebar() {
	if( !is_singular(staff) )
		return;
    // 1. remove the default post sidebar
    remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
    remove_action( 'genesis_sidebar', 'ss_do_sidebar' );
    // add the profile sidebar as defined below
    staff_right_sidebar();

}


function staff_right_sidebar() {
     // check to make sure this is a single profile page
     //echo '<div class="widget"><h2 class="widgettitle">Staff Meta</h2><div class="widget-content">';
    //echo '<p><b>Job Title :</b> '. get_post_meta($post->ID, 'staff_job_title', true).'</p>';
	    echo '<p><b>Job Title:</b> '. the_field('staff_job_title',$post->ID) .'</p>';

        echo the_terms( $post->ID, 'staff_affiliation', '<p><strong>Affiliation: ', ', ', '</strong></p>' );

        echo '<p><strong>Email:</strong> <a href="mailto:'.get_post_meta(get_the_ID(),'staff_email',true).'">'. get_post_meta(get_the_ID(),'staff_email',true).'</a></p>';

       // echo the_terms( $post->ID, 'staffjob', '<p><strong>Job:</strong> ', ', ', '</p>' );

        //echo the_terms( $post->ID, 'stafftitle', '<p><strong>Role:</strong> ', ', ', '</p>' );

        // echo get_the_term_list( $post->ID, 'research_project', '<strong>Research Projects</strong><br/><ul class="projects"><li>', '</li><li>', '</li></ul>' );

        echo '<p><b>Projects:</b></p>';

		$url = site_url();

        echo '<br/><br/><strong><a href="'.$url.'/staff/">Return to staff directory</a></strong>';

        echo  '</div></div>';

}