<?php
/**
 * Custom taxonomy creation class.
 * Override default labels and arguments when calling e.g.
 *
 *          $series_title_args = array('meta_box_cb'=> false);
 *          $series_title_labels = array('single_name'  => 'Series');
 *          $series_title= new \cscs_ns\src\cscs_taxonomies('Series','',$series_title_args,'publication');
 *
 * Date: 2017-10-23
 * Time: 10:00
 */

namespace cscs_ns\src;

class cscs_taxonomies  {

	public $taxonomy;

	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	2017-10-23
	*  @since	0.1.0
	*
	*  @param	$name (string) name for the taxonomy
	*  @param   $labels (array) override the default label with your own values
	*  @param   $args (array) override the default taxonomy arguments
	*  @param   $posts (array) post types to attach taxonomy to
    *  @return  n/a
    *
    */

	public function __construct( $name, $labels=[], $args=[], $posts=[] )
	{
		$this->taxonomy_name  = strtolower( str_replace( ' ', '_', $name ) );
		if ($labels){
		$this->taxonomy_labels = $labels;}else
		{$this->taxonomy_labels=[];}
		if($args){
			$this->taxonomy_args = $args;
		}else
		{$this->taxonomy_args = [];}
		if($posts){$this->posts=$posts;}else{return;}



		// Add action to register the post type, if the post type does not already exist
		if( ! taxonomy_exists( $this->taxonomy_name ) )
		{
			add_action( 'init', array( $this, 'cscs_register_taxonomy' ) );
		}
	}


	/*
	*  cscs_register_taxonomy()
	*
	*  This function will setup the field type data
	*
	*  @type	Action
	*  @date	2017-10-23
	*  @since	0.1.0
	*  @param	n/a
    *  @return  n/a
    *
    */

	public function cscs_register_taxonomy(){

		//Capitilize the words and make it plural
		$name       = ucwords( str_replace( '_', ' ', $this->taxonomy_name ) );
		$plural     = $name . 's';

		// We set the default labels based on the taxonomy name and plural. We overwrite them with the given labels.

		$labels = array_merge(

		// Default
			array(
				'name'                  => _x( $plural, 'taxonomy plural name' ),
				'singular_name'         => _x( $name, 'taxonomy singular name' ),
				'add_new_item'          => __( 'Add New ' . $name ),
				'edit_item'             => __( 'Edit ' . $name ),
				'new_item'              => __( 'New ' . $name ),
				'all_items'             => __( 'All ' . $plural ),
				'view_item'             => __( 'View ' . $name ),
				'items_list'            => __( $plural . 'list'),
				'search_items'          => __( 'Search ' . $plural ),
				'not_found'             => __( 'No ' . strtolower( $plural ) . ' found'),
				'parent_item'           => __('Parent'. $name),
				'parent_item_colon'     => '',
				'menu_name'             => $plural
			),

			// Given labels
			$this->taxonomy_labels

		);
		// Same principle as the labels. We set some defaults and overwrite them with the given arguments.
		$args = array_merge(
		// Default
			array(
				'label'                 => $plural,
				'labels'                => $labels,
				'slug'                  => $plural,
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_nav_menus'     => true,
				'show_admin_column'     => true,
				'show_tagcloud'         => true,
				'meta_box_cb'           => 'post_categories_meta_box'
			),

			// Given args
			$this->taxonomy_args

		);
		try {register_taxonomy($this->taxonomy_name,$this->posts,$args);}
		catch(Exception $e ) {return;}

	}


}