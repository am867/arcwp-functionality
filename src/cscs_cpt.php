<?php
/**
 * Created by PhpStorm.
 * User: dt216
 * Date: 21/09/2017
 * Time: 15:25
 */

namespace cscs_ns\src;

/*
 * References:
 * https://code.tutsplus.com/articles/reusable-custom-meta-boxes-part-1-intro-and-basic-fields--wp-23259
 * https://code.tutsplus.com/articles/custom-post-type-helper-class--wp-25104
 *
 * Initialising the class:
 *
 */


class cscs_cpt {

	public $post_type_name;
	public $post_type_args;
	public $post_type_capabilities;
	public $post_type_labels;
	public $post_type_rewrite;

	/* Class constructor */

	public function __construct( $name, $args = array(), $labels = array(), $capabilities= array() )
	{
		// Set some important variables
		$this->post_type_name           = strtolower( str_replace( ' ', '_', $name ) );
		$this->post_type_args           = $args;
		$this->post_type_capabilities   = $capabilities;
		$this->post_type_labels         = $labels;

		// Add action to register the post type, if the post type does not already exist
		if( ! post_type_exists( $this->post_type_name ) )
		{
			add_action( 'init', array( $this, 'register_post_type' ) );
		}
		// Listen for the save post hook
		$this->save();
	}

	/* Method which registers the post type */
	public function register_post_type()
	{
		//Capitilize the words and make it plural
		$name       = ucwords( str_replace( '_', ' ', $this->post_type_name ) );
		$plural     = $name . 's';

		// We set the default labels based on the post type name and plural. We overwrite them with the given labels.
		$labels = array_merge(
		// Default
			array(
				'name'                  => _x( $plural, 'post type general name' ),
				'singular_name'         => _x( $name, 'post type singular name' ),
				'add_new'               => __( 'Add New ' . $name ),
				'add_new_item'          => __( 'Add New ' . $name ),
				'edit_item'             => __( 'Edit ' . $name ),
				'new_item'              => __( 'New ' . $name ),
				'all_items'             => __( 'All ' . $plural ),
				'view_item'             => __( 'View ' . $name ),
				'search_items'          => __( 'Search ' . $plural ),
				'not_found'             => __( 'No ' . strtolower( $plural ) . ' found'),
				'not_found_in_trash'    => __( 'No ' . strtolower( $plural ) . ' found in Trash'),
				'parent_item_colon'     => '',
				'menu_name'             => $plural
			),

			// Given labels
			$this->post_type_labels

		);
		$capabilities = array_merge(
			array(
			'edit_post'             => 'edit_'.$name,
			'delete_post'           => 'delete_'.$name,
			'delete_private_posts'  => 'delete_private_'.$name,
			'delete_published_posts'=> 'delete_published_'.$name,
			'delete_others_posts'   => 'delete_others_'.$name,
			'edit_others_posts'     => 'edit_others_'.$name,
			'edit_private_posts'    => 'edit_private_'.$name,
			'edit_published_posts'  => 'edit_published_'.$name,
			'publish_posts'         => 'publish_'.$name,
			'read_private_posts'    => 'read_private_'.$name,
		),

			// Given capabilities
			$this->post_type_capabilities
		);
		$rewrite = array(
			'slug'                  => $name,
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);


		// Same principle as the labels. We set some defaults and overwrite them with the given arguments.
		$args = array_merge(

		// Default
			array(
				'label'                 => $plural,
				'labels'                => $labels,
				'slug'                  => $plural,
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'supports'              => array( 'title','excerpt','editor','thumbnail','author','excerpt','revisions','genesis-cpt-archives-settings' ),
				'show_in_nav_menus'     => true,
				'show_in_admin_bar'     => true,
				'_builtin'              => false,
                'capability_type'       => array( strtolower($name), strtolower($plural) ),
				'capabilities'          => $capabilities,

				'rewrite'               => $rewrite,
			),

			// Given args
			$this->post_type_args
            //array()

		);

		// Register the post type
		register_post_type( $this->post_type_name, $args );
		flush_rewrite_rules( false );
	}
	
	/* Listens for when the post type being saved */
	public function save()
	{
		// Need the post type name again
		$post_type_name = $this->post_type_name;

		add_action( 'save_post',
			function() use( $post_type_name )
			{
				// Deny the WordPress autosave function
				if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
                if(!isset($_POST['custom_post_type'])) :
                    $_POST['custom_post_type'] = '';
                endif;
				if ( ! wp_verify_nonce( $_POST['custom_post_type'], plugin_basename(__FILE__) ) ) return;

				global $post;

				if( isset( $_POST ) && isset( $post->ID ) && get_post_type( $post->ID ) == $post_type_name )
				{
					global $custom_fields;

					// Loop through each meta box
					foreach( $custom_fields as $title => $fields )
					{
						// Loop through all fields
						foreach( $fields as $label => $type )
						{
							$field_id_name  = strtolower( str_replace( ' ', '_', $title ) ) . '_' . strtolower( str_replace( ' ', '_', $label ) );

							update_post_meta( $post->ID, $field_id_name, $_POST['custom_meta'][$field_id_name] );
						}

					}
				}
			}
		);
	}
}