<?php
/**
 * Created by PhpStorm.
 * User: dt216
 * Date: 13/12/2017
 * Time: 14:30
 */
if ( ! function_exists( 'programme_taxonomy' ) ) {

// Register Custom Taxonomy
	function programme_taxonomy() {

		$labels = array(
			'name'                       => _x( 'Programmes', 'Taxonomy General Name', 'cscs_ns' ),
			'singular_name'              => _x( 'Programme', 'Taxonomy Singular Name', 'cscs_ns' ),
			'menu_name'                  => __( 'Parent Programmes', 'cscs_ns' ),
			'all_items'                  => __( 'All Programmes', 'cscs_ns' ),
			'parent_item'                => __( 'Parent Programme', 'cscs_ns' ),
			'parent_item_colon'          => __( 'Parent Programme:', 'cscs_ns' ),
			'new_item_name'              => __( 'New Programme Name', 'cscs_ns' ),
			'add_new_item'               => __( 'Add New Programme', 'cscs_ns' ),
			'edit_item'                  => __( 'Edit Programme', 'cscs_ns' ),
			'update_item'                => __( 'Update Programme', 'cscs_ns' ),
			'view_item'                  => __( 'View Programme', 'cscs_ns' ),
			'separate_items_with_commas' => __( 'Separate programmes with commas', 'cscs_ns' ),
			'add_or_remove_items'        => __( 'Add or remove programmes', 'cscs_ns' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'cscs_ns' ),
			'popular_items'              => __( 'Popular Programmes', 'cscs_ns' ),
			'search_items'               => __( 'Search Programmes', 'cscs_ns' ),
			'not_found'                  => __( 'Not Found', 'cscs_ns' ),
			'no_terms'                   => __( 'No programmes', 'cscs_ns' ),
			'items_list'                 => __( 'Programmes list', 'cscs_ns' ),
			'items_list_navigation'      => __( 'Programmes list navigation', 'cscs_ns' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
            'rewrite'                    => array (
                                                'slug' => 'programme',
                                                'with_front' => false
                                            )
		);
		register_taxonomy( 'programme', array( 'project','publication' ), $args );

	}
	add_action( 'init', 'programme_taxonomy', 0 );
}