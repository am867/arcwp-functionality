<?php
/**
 * Staff CPT taxonomies
 *
 * This file registers taxonomies associated with the staff CPT. Add them as necessary
 *
 * @package      Staff CPT and shortcode
 * @since        0.1.6
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
 */

if ( ! function_exists( 'add_staff_taxonomies' ) ) {
    function add_staff_taxonomies()
    {

        /**
         * Division taxonomy
         */

//        $labels = array(
//
//            'name' => _x('Affiliations', 'taxonomy general name', 'staff_cpt'),
//            'singular_name' => _x('Affiliation', 'taxonomy singular name', 'staff_cpt'),
//            'search_items' => __('Search Affiliations', 'staff_cpt'),
//            'all_items' => __('All Affiliations', 'staff_cpt'),
//            'edit_item' => __('Edit DAffiliation', 'staff_cpt'),
//            'update_item' => __('Update Affiliation', 'staff_cpt'),
//            'add_new_item' => __('Add Affiliation', 'staff_cpt'),
//            'new_item_name' => __('New Affiliation', 'staff_cpt'),
//            'menu_name' => __('Affiliationss', 'staff_cpt'),
//        );
//        $rewrite = array(
//            'slug' => 'affiliations',
//            'with_front' => true,
//            'hierarchical' => false,
//        );
//        $args = array(
//            'labels'                     => $labels,
//            'hierarchical'               => true,
//            'public'                     => true,
//            'show_ui'                    => true,
//            'show_admin_column'          => true,
//            'show_in_nav_menus'          => true,
//            'show_tagcloud'              => true,
//            'rewrite'      => array('slug' => 'staff/division', 'with_front' => false)
//
//        );
//        register_taxonomy('staff_affiliation', array('staff','publication'), $args);

        /*
         * Register Staff Role Taxonomy
         */

        $labels = array(
            'name' => _x('Role', 'taxonomy general name', 'staff_cpt'),
            'singular_name' => _x('Role', 'taxonomy singular name', 'staff_cpt'),
            'search_items' => __('Roles', 'staff_cpt'),
            'all_items' => __('All Roles', 'staff_cpt'),
            'edit_item' => __('Edit Roles', 'staff_cpt'),
            'update_item' => __('Update Roles', 'staff_cpt'),
            'add_new_item' => __('Add New Role', 'staff_cpt'),
            'new_item_name' => __('New Role', 'staff_cpt'),
            'menu_name' => __('Roles', 'staff_cpt'),
        );
        $rewrite = array(
            'slug' => 'staff-role',
            'with_front' => true,
            'hierarchical' => false,
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        );
        register_taxonomy('staff_role', array('staff'), $args);

        /*
         * Add Research Project Taxonomy
         */

//        $labels = array(
//            'name'                       => _x( 'Research Projects', 'Taxonomy General Name', 'staff_cpt' ),
//            'singular_name'              => _x( 'Research Project', 'Taxonomy Singular Name', 'staff_cpt' ),
//            'menu_name'                  => __( 'Research Projects', 'staff_cpt' ),
//            'all_items'                  => __( 'All Research Projects', 'staff_cpt' ),
//            'parent_item'                => __( 'Parent Item', 'staff_cpt' ),
//            'parent_item_colon'          => __( 'Parent Item:', 'staff_cpt' ),
//            'new_item_name'              => __( 'New Research Project Name', 'staff_cpt' ),
//            'add_new_item'               => __( 'Add New Research Project', 'staff_cpt' ),
//            'edit_item'                  => __( 'Edit Research Project', 'staff_cpt' ),
//            'update_item'                => __( 'Update Project', 'staff_cpt' ),
//            'view_item'                  => __( 'View Project', 'staff_cpt' ),
//            'separate_items_with_commas' => __( 'Separate projects with commas', 'staff_cpt' ),
//            'add_or_remove_items'        => __( 'Add or remove projects', 'staff_cpt' ),
//            'choose_from_most_used'      => __( 'Choose from the most used', 'staff_cpt' ),
//            'popular_items'              => __( 'Popular Projects', 'staff_cpt' ),
//            'search_items'               => __( 'Search Projects', 'staff_cpt' ),
//            'not_found'                  => __( 'Not Found', 'staff_cpt' ),
//            'no_terms'                   => __( 'No projects', 'staff_cpt' ),
//            'items_list'                 => __( 'Projects list', 'staff_cpt' ),
//            'items_list_navigation'      => __( 'Projects list navigation', 'staff_cpt' ),
//        );
//        $rewrite = array(
//            'slug' => 'research-project',
//            'with_front' => true,
//            'hierarchical' => false,
//        );
//        $args = array(
//            'labels'                     => $labels,
//            'hierarchical'               => true,
//            'public'                     => true,
//            'show_ui'                    => true,
//            'show_admin_column'          => true,
//            'show_in_nav_menus'          => true,
//            'show_tagcloud'              => true,
//        );
//        register_taxonomy( 'research_project', array( 'staff' ), $args );

    }//close function

    add_action( 'init', 'add_staff_taxonomies', 0 );

}