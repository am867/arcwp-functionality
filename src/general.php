<?php
/**
 * General Functions
 *
 * This file registers metaboxes using Advanced Custom Fields. Add them as necessary
 *
 * @package      Staff CPT and shortcode
 * @since        0.1.0
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
 *
 * 1. Load CSS overrides
 * 2. Set-up CPT template paths
 * 3. Add to list of allowed Staff single page redirects
 * 4. Implement Staff CPT sidebar
 * 5. Adds new image dimensions for staff CPT mug shot
 * 6. Set-up WPFacet caching
 * 7. Create bidirectional updates between ACF relationship fields with the same name
 * 8. Project archive stuff
 * 9. Change the year dropdown facet for "year" to descending
 *
 */

// 1. Load CSS style overrides
add_action( 'wp_enqueue_scripts', 'jobs_enqueue_styles' );
function jobs_enqueue_styles() {
	wp_enqueue_style(
		'staff',
		plugin_dir_url( __FILE__ ) . 'staff-facet.css', array(),
		FALSE
	);
	wp_enqueue_style(
		'override',
		plugin_dir_url( __FILE__ ) . 'css-override.css',
		array(),
		FALSE
	);
    
    wp_register_script(
        'slick',
        plugins_url( 'js/slick.min.js', dirname(__FILE__) ),
        array('jquery'),
        '1.0',
        TRUE
    );
    
    wp_register_script( 
        'cscs-lang-jump',
        plugins_url( 'js/language-jump.js', dirname(__FILE__) ),
        array('jquery', 'slick'),
        '1.0',
        TRUE
    );    
    
    $protocol = isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
    $params_lang = array(
        'ajaxurl' => admin_url( 'admin-ajax.php', $protocol ),
        'siteurl' => esc_url( home_url() )
    );
    wp_localize_script( 'cscs-lang-jump', 'cscs_ajax', $params_lang);
    wp_enqueue_script(
        'slick'
    );
    wp_enqueue_script(
        'cscs-lang-jump'
    );
}
//2. Page templates
/**
 * Set-up CPT page templates
 **/


add_filter( 'template_include', 'include_template_function', 1 );
function include_template_function( $template ) {
    if ( get_post_type() == 'staff' ) {

        if ( is_single() ) {
            $template = ( plugin_DIR . '/templates/single-staff.php' );
        }

        
        elseif ( is_tax('research_project') || is_tax('staff_affiliation')) {
            $template = ( plugin_DIR . '/templates/taxonomy-archive.php' );
        }


        elseif ( is_archive() ) {
           $template = ( plugin_DIR . '/templates/archive-staff.php' );
        }
    }
    elseif ( get_post_type() == 'project' ) {

	    if ( is_single() ) {
	    	$template = ( plugin_DIR . '/templates/single-project.php' );
	    }
        
         elseif( is_tax( 'programme' ) ) {
        $template = ( plugin_DIR . '/templates/taxonomy-programme.php' );
    }

	    elseif ( is_archive() ) {
		    $template = ( plugin_DIR . '/templates/archive-projects.php' );
	    }
	}
	elseif ( get_post_type() == 'test' ) {

	    if ( is_single() ) {
			$template = ( plugin_DIR . '/templates/single-test.php' );
	    }

	    elseif ( is_archive() ) {
		    $template = ( plugin_DIR . '/templates/archive-test.php' );
	    }
    }
    elseif(is_page('frequently-asked-questions')){
	    $template = ( plugin_DIR . '/templates/faq-page-template.php' );
    }
    elseif( is_front_page() ) {
        $template = ( plugin_DIR . '/templates/front-page.php' );
    }
     elseif( is_tax( 'programme' ) ) {
        $template = ( plugin_DIR . '/templates/taxonomy-programme.php' );
    }
   

    return $template;
}

//3. Add allowed redirect hosts
// TODO: add to site options panel

add_filter( 'allowed_redirect_hosts' , 'my_allowed_redirect_hosts' , 10 );
function my_allowed_redirect_hosts($content){
	$content[] = 'www.phpc.cam.ac.uk';
	$content[] = 'www.iph.cam.ac.uk';
	$content[] = 'www.cimr.cam.ac.uk';
	$content[] = 'radiology.medschl.cam.ac.uk';
	$content[] = 'www.psychiatry.cam.ac.uk';
	$content[] = 'www.medicine.dev';
	// wrong: $content[] = 'http://codex.example.com';
	return $content;
}


// 4. Set-up Staff Sidebar
/**
 * Add a customisable right side bar to the single staffpost's edit screen and layout
 * Don't forget to include the sidebar metabox definitions
 */
function staff_sidebar() {
    remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
    remove_action( 'genesis_sidebar', 'ss_do_sidebar' );

    if ( is_singular( 'staff' ) ) {
        be_right_sidebar();
    }
}

add_action( 'genesis_sidebar', 'staff_sidebar' );
/**
 * Right Sidebar
 *
 */
function be_right_sidebar() {
	if( !is_singular() )
		return;

	$widgets = get_post_meta( get_the_ID(), 'be_right_sidebar', true );
	if( empty( $widgets ) )
		return;

	$count = 0;
	while( $count < $widgets ) {
		$title = esc_attr( get_post_meta( get_the_ID(), 'be_right_sidebar_' . $count . '_title', true ) );
		$image = '';
		$image_id = get_post_meta( get_the_ID(), 'be_right_sidebar_' . $count . '_image', true );
		if( $image_id && function_exists( 'ea_image_resize' ) ):
			$image = ea_image_resize( $image_id, null, 349, 126, true );
			$image = '<img src="' . $image['url'] . '" />';
		endif;
		$content = get_post_meta( get_the_ID(), 'be_right_sidebar_' . $count . '_content', true );
		$background = esc_attr( get_post_meta( get_the_ID(), 'be_right_sidebar_' . $count . '_background', true ) );
		$menu = get_post_meta( get_the_ID(), 'be_right_sidebar_' . $count . '_menu', true );
		if( $menu ) {
			$menu = wp_nav_menu( array( 'menu' => $menu, 'echo' => false ) );
			if( $menu )
				$content .= $menu;
		}
		$rss = esc_url( get_post_meta( get_the_ID(), 'be_right_sidebar_' . $count . '_rss', true ) );
		if( $rss ) {
			$rss_number = get_post_meta( get_the_ID(), 'be_right_sidebar_' . $count . '_rss_number', true );
			if ( empty( $rss_number ) ) {
				$rss_number = '5';
			}
			$maxitems = 0;
			$output = '';
			$rss = fetch_feed( $rss );
			if( !is_wp_error( $rss ) ):
				$maxitems = $rss->get_item_quantity( $rss_number );
				$rss_items = $rss->get_items( 0, $maxitems );
			endif;
			if( 0 !== $maxitems ):
				$output .= '<ul>';
				foreach( $rss_items as $item ):
					$output .= '<li><a href="' . esc_url( $item->get_permalink() ) . '">' . esc_html( $item->get_title() ) . '</a></li>';
				endforeach;
				$output .= '</ul>';
			endif;
			$content .= $output;
		}

		echo '<div class="widget ' . $background . '">';
		if( $title )
			echo '<h4 class="widgettitle">' . $title . '</h4>';
		echo $image;
		echo '<div class="widget-content">' . apply_filters( 'the_content', $content ) . '</div>';
		echo '</div>';
		$count++;
	}
}


// 5. Adds new image dimensions for staff CPT mug shot

add_image_size ( 'Square', 250, 250, TRUE );


// 6. Set-up WPFacet caching

function my_cache_lifetime( $seconds ) {
    return 86400; // one day
}
//add_filter( 'facetwp_cache_lifetime', 'my_cache_lifetime' );

add_filter('pre_get_posts', 'sort_my_archive');
function sort_my_archive($q) {
	if ($q->is_post_type_archive('staff')) {
		$q->set('meta_key', 'staff_surname');
		$q->set('orderby', 'meta_value');
		$q->set('order', 'ASC');
	}
	return $q;
}


// 7.Create bidirectional updates between ACF relationship fields with the same name


function bidirectional_acf_update_value( $value, $post_id, $field  ) {

	// vars
	$field_name = $field['name'];
	$field_key = $field['key'];
	$global_name = 'is_updating_' . $field_name;


	// bail early if this filter was triggered from the update_field() function called within the loop below
	// - this prevents an inifinte loop
	if( !empty($GLOBALS[ $global_name ]) ) return $value;


	// set global variable to avoid inifite loop
	// - could also remove_filter() then add_filter() again, but this is simpler
	$GLOBALS[ $global_name ] = 1;


	// loop over selected posts and add this $post_id
	if( is_array($value) ) {

		foreach( $value as $post_id2 ) {

			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);


			// allow for selected posts to not contain a value
			if( empty($value2) ) {

				$value2 = array();

			}


			// bail early if the current $post_id is already found in selected post's $value2
			if( in_array($post_id, $value2) ) continue;


			// append the current $post_id to the selected post's 'related_posts' value
			$value2[] = $post_id;


			// update the selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);

		}

	}


	// find posts which have been removed
	$old_value = get_field($field_name, $post_id, false);

	if( is_array($old_value) ) {

		foreach( $old_value as $post_id2 ) {

			// bail early if this value has not been removed
			if( is_array($value) && in_array($post_id2, $value) ) continue;


			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);


			// bail early if no value
			if( empty($value2) ) continue;


			// find the position of $post_id within $value2 so we can remove it
			$pos = array_search($post_id, $value2);


			// remove
			unset( $value2[ $pos] );
			// update the un-selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);

		}

	}


	// reset global varibale to allow this filter to function as per normal
	$GLOBALS[ $global_name ] = 0;


	// return
	return $value;

}

add_filter('acf/update_value/name=project_staff', 'bidirectional_acf_update_value', 10, 3);
add_filter('acf/update_value/name=staff_publications', 'bidirectional_acf_update_value', 10, 3);



// 8. Project archive stuff

//* Add Archive Settings option to Project CPT
add_post_type_support( 'project', 'genesis-cpt-archives-settings' );

//* Define a custom image size for images on Portfolio archives
add_image_size( 'project', 500, 300, true );

add_action( 'pre_get_posts', 'change_project_posts_per_page' );
/**
 * Set all the entries to appear on Project archive page
 *
 * @link http://www.billerickson.net/customize-the-wordpress-query/
 * @param object $query data
 *
 */
function change_project_posts_per_page( $query ) {

	if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'project' ) ) {
		$query->set( 'posts_per_page', '-1' );
	}

}
// 9. Change the year dropdown facet for "year" to descending


add_filter( 'facetwp_facet_orderby', function( $orderby, $facet ) {
	if ( 'publication_year' == $facet['name'] ) {
		$orderby = 'f.facet_display_value+0 DESC';
	}
	return $orderby;
}, 10, 2 );


/* Replace Altitude Pro Google Font */
// get rid of the old theme-defined font
function cscs_arc_remove_font() {
	wp_dequeue_style( 'altitude-google-fonts' );
}
add_action('wp_print_styles','cscs_arc_remove_font');
// and add in the new font
function cscs_add_font(){
	$my_theme = wp_get_theme();
	wp_enqueue_style( 'altitude-google-fonts-open-sans', '//fonts.googleapis.com/css?family=Open+Sans:200,800', array(), $my_theme->get( 'Version' ) );
}
add_action('wp_print_styles','cscs_add_font');

/* Remove the category meta from posts */
if (!(is_admin() )) {
	add_filter('the_category', 'no_links', 45);
	function no_links(){
		echo " ";
	}
}

/* Move the date from above the title to below */
// Remove from above the post title
add_filter( 'genesis_post_info', 'cscs_post_info_filter' ,20);
function cscs_post_info_filter( $post_info ) {
	$post_info = '';
	return $post_info;
}
// Add back in under the title

add_action( 'genesis_before_entry_content', 'cscs_post_date', 90 );
function cscs_post_date(){
	if ( ! post_type_supports( get_post_type(), 'genesis-entry-meta-before-content' ) ||is_singular('test') || is_singular('staff') || is_singular('project')) {
		return;
	}
	$post_info = '[post_date]';
	printf( '<p class="entry-meta" style="text-align: center">%s</p>', do_shortcode( $post_info ) );

}
/*
 * Replace the wpfacet image size with our custom size
 * Note that image width will depend on the container size
 * n.b. this may not be needed with a coming version
 */

add_action('init','cscs_wpfacet_image');
function cscs_wpfacet_image(){
	add_filter( 'facetwp_builder_item_value', function ( $value, $item ) {
		if ( 'featured_image' == $item['source'] && ! empty( $value ) ) {
			$value = '<a href="'.get_permalink(get_the_ID()).'">'.get_the_post_thumbnail( get_the_ID(), 'project-list' ).'</a>';
		}
		return $value;
	}, 10, 2 );
}

/*
 * Add custom body class .wpfacet to pages using [facetwp] shortcode
 */

function facetwp_body_class( $c ) {
	global $post;
	if( isset($post->post_content) && (has_shortcode( $post->post_content, 'facetwp' ) || has_shortcode( $post->post_content, 'stafflist' ))) {
		$c[] = 'wpfacet';
	}
	return $c;
}
add_filter( 'body_class', 'facetwp_body_class' );

/*
 * Enforce post title
 */
add_action( 'admin_enqueue_scripts', 'post_title_validation_function' );

function post_title_validation_function($hook){
	if( 'post.php' != $hook && 'edit.php' != $hook && 'post-new.php' != $hook)
		return;
	wp_enqueue_script( 'post_title_check_script',  plugins_url( '/',dirname(__FILE__ ))  . 'js/post-title-validation.js', array( 'jquery' ), '1', true );
}

// Allow short codes in custom post type archive intro. Text
add_filter( 'genesis_cpt_archive_intro_text_output', 'do_shortcode', 20 );
add_filter( 'genesis_term_intro_text_output', 'do_shortcode', 20 );
