<?php
/**
 * CSS Overrides
 * User: dt216
 * Date: 06/07/2017
 * Time: 14:16
 */
namespace cscs_ns\src;

class Override_CSS
{

	/*
	*  __construct
	*
	*  @type	function
	*  @date	2017-11-1
	*  @since	0.1.0
	*
	*  @param	$css_args (array) CSS handle and file name
    *  @return  n/a
    *
    */

    public function __construct( $css_args = array() ) {
        // if the arguments are passed, hook the CSS overrides
	    if (!$css_args){ return;}
	    $this->css_list        = $css_args;
	    add_action( 'wp_enqueue_scripts', array($this, 'site_override_style'),200  );
    }
	/*
		*  site_override_style()
		*
		*  This function registers and enqueues passed CSS files
		*
		*  @type	action
		*  @date	2017-11-1
		*  @since	 0.1.13
		*
		*  @param	n/a
		*  @return  n/a
		*
		*/
    public function site_override_style() {

    foreach ($this->css_list as $css)
	{
		$myStyleFile = plugins_url($css[1], __FILE__ ) ;
	    wp_register_style($css[0],  $myStyleFile);
	    wp_enqueue_style($css[0]);
    }
}}