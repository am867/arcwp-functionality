<?php
/**
 * Created by PhpStorm.
 * User: dt216
 * Date: 19/11/2018
 * Time: 10:13
 */

namespace cscs_ns\src;

class CSCS_CPT_Widget extends \WP_Widget {

	// Main constructor
	public function __construct() {
		parent::__construct(
			'cscs_cpt_listing_widget',
			__( 'CSCS CPT Listing Widget', 'text_domain' ),
			array(
				'customize_selective_refresh' => true,
			)
		);
	}

	// The widget form (for the backend )
	public function form( $instance ) {
		// Set widget defaults
		$defaults = array(
			'title'    => '',
			'text'     => '',
			'textarea' => '',
			'checkbox' => '',
			'select'   => '',
		);

		// Parse current settings with defaults
		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

		<?php // Widget Title ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<?php // Text Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php _e( 'Text:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
		</p>

		<?php // Textarea Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'textarea' ) ); ?>"><?php _e( 'Textarea:', 'text_domain' ); ?></label>
			<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'textarea' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'textarea' ) ); ?>"><?php echo wp_kses_post( $textarea ); ?></textarea>
		</p>

		<?php // Checkbox ?>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'checkbox' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'checkbox' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $checkbox ); ?> />
			<label for="<?php echo esc_attr( $this->get_field_id( 'checkbox' ) ); ?>"><?php _e( 'Checkbox', 'text_domain' ); ?></label>
		</p>

		<?php // Dropdown ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'select' ); ?>"><?php _e( 'Select', 'text_domain' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'select' ); ?>" id="<?php echo $this->get_field_id( 'select' ); ?>" class="widefat">
				<?php
				// Your options array
				$options = array(
					''        => __( 'Select', 'text_domain' ),
					'option_1' => __( 'Option 1', 'text_domain' ),
					'option_2' => __( 'Option 2', 'text_domain' ),
					'option_3' => __( 'Option 3', 'text_domain' ),
				);

				// Loop through options and add each one to the select dropdown
				foreach ( $options as $key => $name ) {
					echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select, $key, false ) . '>'. $name . '</option>';

				} ?>
			</select>
		</p>
		<?php
	}

	// Update widget settings
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['text']     = isset( $new_instance['text'] ) ? wp_strip_all_tags( $new_instance['text'] ) : '';
		$instance['textarea'] = isset( $new_instance['textarea'] ) ? wp_kses_post( $new_instance['textarea'] ) : '';
		$instance['checkbox'] = isset( $new_instance['checkbox'] ) ? 1 : false;
		$instance['select']   = isset( $new_instance['select'] ) ? wp_strip_all_tags( $new_instance['select'] ) : '';
		return $instance;
	}

	// Display the widget
	public function widget( $args, $instance ) {
		extract( $args );

		// Check the widget options
		$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
		$text     = isset( $instance['text'] ) ? $instance['text'] : '';
		$textarea = isset( $instance['textarea'] ) ?$instance['textarea'] : '';
		$select   = isset( $instance['select'] ) ? $instance['select'] : '';
		$checkbox = ! empty( $instance['checkbox'] ) ? $instance['checkbox'] : false;

		// WordPress core before_widget hook (always include )
		echo $before_widget;

		// Display the widget
		echo '<div class="widget-text wp_widget_plugin_box">';

		// Display widget title if defined
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		$args = array(
			'post_type' => 'project',
			'meta_query'	=> array(
				// check to see if end date has been set
				array(
					'key'		=> 'promote_to_homepage',
					'value'	    => '1',
					'compare' => 'LIKE'
				)
			),
		);

		$custom_posts = new \WP_Query($args);
		// The Loop
		if ( $custom_posts->have_posts() ) {
			echo '<div class="project-home-block">';
			while ( $custom_posts->have_posts() ) {
				$custom_posts->the_post();
				echo '<div class="project-item">';

				$imageLabel = get_field('project_recruitment');

				if(has_post_thumbnail()){
					$image_mobile = wp_get_attachment_image_src( get_post_thumbnail_id(  get_the_ID() ), 'homepage-thumb' )[0];
					echo '<div class="project-image">';
					echo '<a href="'.get_permalink().'">';
					echo '<img src="'.$image_mobile.'" class="project-image-img" />';
                    if( $imageLabel && 'yes' === $imageLabel ) :
					   echo '<span class="captions">Take part</span>';
                    endif;
					echo '</a>';
					echo '</div>';
				}

				echo '<div class="project-summary">';
				echo '<a href="'.get_permalink().'">';
				echo '<h2>' . get_the_title() . '</h2>';
				echo '</a>';

				//$text = get_field('project_content');
				$text = get_the_excerpt();

				// show the full, untruncated excerpt. Leave the code in because they might reverse this...

				// if ( '' != $text ) {
				// 	$text = strip_shortcodes( $text );
				// 	$text = apply_filters('the_content', $text);
				// 	$text = str_replace(']]&gt;', ']]&gt;', $text);
				// 	$excerpt_length = 20; // 20 words
				// 	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
				// 	$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
				// }
				// $text = apply_filters('the_excerpt', $text);

				echo '<p>'.$text.'</p>';

				echo '</div>';
                echo '<a href="'.get_permalink().'" class="button small">More info</a>';
				echo '</div>';
			}
			echo '</div>';
		} else {
			// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();

		// // Display text field
		// if ( $text ) {
		//     echo '<p>' . $text . '</p>';
		// }

		// // Display textarea field
		// if ( $textarea ) {
		//     echo '<p>' . $textarea . '</p>';
		// }

		// // Display select field
		// if ( $select ) {
		//     echo '<p>' . $select . '</p>';
		// }

		// // Display something if checkbox is true
		// if ( $checkbox ) {
		//     echo '<p>Something awesome</p>';
		// }

		echo '</div>';

		// WordPress core after_widget hook (always include )
		echo $after_widget;
	}

}