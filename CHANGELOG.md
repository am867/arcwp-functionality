# ARC Functionality Change Log
##[1.8.9] - 2018-12-19
### Added
- Staff CRSID field
##[1.8.5] - 2018-12-13
### Changed
- removed loops from staff cpt archive template
### Added
- allow short codes in CPT archive intro 
##[1.8.4] - 2018-12-12
### Added
- Added Test Languages
##[1.8.3] - 2018-12-12
### Added
- Test archive body class
##[1.8.2] - 2018-12-11
### Changed
- Tests archive page to use WP Facets
- Logic for displaying archive page intro text
##[1.8.01] - 2018-12-7
### Changed
- temporarily replace ACF file upload field with an URL field
##[1.8.00] - 2018-12-7
### Added
-  download ACF Repeater field to Test posts
### Changed
- Single Test post template to use new ACF field
##[1.7.12] - 2018-12-5
### Added
- Added Test Download ACF repeater field
##[1.7.11] - 2018-11-27
### Changed
- Added link to staff image shortcode grid
##[1.7.10] - 2018-11-27
### Changed
- Added link to FacetWP image size
##[1.7.9] - 2018-11-26
### Added 
- Check for null recruitment details on single project template
##[1.7.8] - 2018-11-26
### Added 
- Project recruitment type project taxonomy
- Project recruitment types to single project edit screen
- Project recruitment details to single project edit screen
- Project recruitment details to project single template
##[1.7.6-7] - 2018-11-22
### Added 
- Staff HTML content sanitisation
##[1.7.5] - 2018-11-22
### Added 
- ACF boolean switch to hide projects on singe staff posts
##[1.7.4] - 2018-11-21
### Added 
- ACF boolean field to link to directory on singgle staff page
##[1.7.3] - 2018-11-21
### Added 
- Check for staff job title
##[1.7.2] - 2018-11-21
### Added
- Staff link to directory field
##[1.7.1] - 2018-11-19
### Changed
- Single staff template portrait size to Medium
- Adds job title css class on Single staff template 
##[1.7.0] - 2018-11-19
### Added
- CSCS CPT widget code
##[1.6.0] - 2018-11-16
### Added
- WP Media Offload MIME type filter
### Changed
- Plugin dependency checks
##[1.5.25] - 2018-11-14
### Changed
- Disabled programme taxonomy visibility
##[1.5.24] - 2018-11-14
### Changed
- FAQ classes
- icon to button
##[1.5.23] - 2018-11-14
### Added
- New CSS classes for project FacetWP facets
##[1.5.22] - 2018-11-13
### Added
- Added Project recruitment status 
##[1.5.21] - 2018-11-12
### Added
- Added Project status facet filter to Project archive
##[1.5.20] - 2018-11-12
### Added
- Staff grid clear parameter
##[1.5.19] - 2018-11-12
### Changed
- staff listing on single project template changed to unordered list
##[1.5.18] - 2018-11-9
### Changed
- min-height  .bw-featured-title to 400px
##[1.5.17] - 2018-11-8
### Changed
- Keyword category made hierarchical
##[1.5.16] - 2018-11-8
### Changed
- Removes date from single staff and project pages
##[1.5.15] - 2018-11-8
### Changed
- Staff project listing to UL list
- Removed spacing between Project and Publication listing**
##[1.5.14] - 2018-11-8
### Changed
- Staff template layout fix - removed extraneous <div> elements
##[1.5.13] - 2018-11-7
### Added
- Removed redundant code from staff archive template
##[1.5.12] - 2018-11-7
### Added
- More Staff shortcode classes
##[1.5.11] - 2018-11-6
### Changed
- Make stafflist pages full width
##[1.5.10] - 2018-11-6
### changed
- Added a default silhouette for staff profile listing
- Reassigned staff short-code image parameter
### Added a new staff listing image size
##[1.5.9] - 2018-11-2
### changed
- adjust page title
##[1.5.8] - 2018-11-2
### changed
- Removed title from archive page featured image
##[1.5.7] - 2018-11-2
### changed
- wpfacet bodyclass now added on posts where facetwp shortcode is used, rather than hardcoded pages
##[1.5.6] - 2018-11-2
### Added
- wpfacet body class for specified pages
##[1.5.5] - 2018-11-2
### Changed
- All WPFacet images are now resized to project-list size
##[1.5.4] - 2018-11-1
### Changed
- Removes title in featured image
##[1.5.3] - 2018-11-1
### Added
- Code to move date from above title to above content on posts
##[1.5.2] - 2018-10-31
### Added
- Code to remove post category meta
## [1.5.1] - 2018-10-30
### Added
- New Image size for project listing grid
## [1.5.0] - 2018-10-17
### Changed
- Removed JS Masonry Project Archive template
### Added
- Project CPT archive using FacetWP facets and templates
## [1.4.3] - 2018-10-17
### Changed
- Removed fixed width for title container on Archive page title
## [1.4.2] - 2018-10-17
### Changed
- removed bottom border from archive title
## [1.4.0-1] - 2018-10-16
### Changed
- Replace parent them Google Font with Open Sans
## [1.3.4] - 2018-10-12
### Changed
- Removed JSON file from GIT ignore file (doh!)
## [1.3.3] - 2018-10-12
### Added
- New hooks to inject post title in correct structural wrap
## [1.3.2] - 2018-10-9
### Added
- Adds a default image on project archive if no featured image
## [1.3.1] - 2018-10-8
### Changed
- Project grid image size -> large
## [1.3.0] - 2018-10-4
### Added
- Image header to CPT archives 
- Enables Genesis Archive settings on all CPTs
## [1.2.9] - 2018-10-1
### Changed
- Removes autoloader check
- Amends JSON for Project CPT metaboxes to show Featured Image field
- Start reworking custom capability code for CPT 
## [1.2.8] - 2018-09-19
### Changed
- Corrects CPT capability labels
## [1.2.7] - 2018-09-18
### Changed
- Removes spurious "version" parameter when enqueuing style sheets
## [1.2.6] - 2018-09-10
### Changed
- Added Tests CPT
- Added tests single and archive pages
## [1.2.5] - 2018-08-29
### Changed
- Remove Project heading on single staff template if no projects
## [1.2.4] - 2018-08-29
### Changed
- Don't show Projects title if no projects
## [1.2.3] - 2018-08-28
### Changed
- Added check for existence of Autoloader class
## [1.2.2] - 2018-08-17
### Changed
- Added new thumbnail size 'homepage-thumb' for consistent square images for projects.
- Added featured image support to template via Gensis hooks
- Added 'promote to homepage', 'project label' fields to ACF for Project
- Added 'staffcategory' shortcode to display title of role
- Updated 'stafflist' shortcode to support 'image' thumbnail via parameter
- Updated archive-projects.php page to use new thumbnail size 'homepage-thumb' for it's images and include a default image.
## [1.2.1] - 2018-07-11
### Changed
- Correction of typos in Publications ACF field
## [1.2.0] - 2018-07-11
### Added
- FAQ page template
## [1.1.15/16] - 2018-07-10
### Changed
- Single Staff template modified
## [1.1.14] - 2018-02-14
### Changed
- Corrected staff_affiliation taxonomy ACF field to write back from ACF metabox to taxonomy
## [1.1.12] - 2018-02-12
### Changed
- Removed staff sidebar, made staff page full width
### Added
- Associated projects on Staff single temple and role
## [1.1.11] - 2018-02-12
### Added
- Site Options Admin panel
## [1.1.10] - 2018-02-12
### Changed
- Removed 1MB minimum on staff image upload size
## [1.1.9] - 2018-02-09
### Added
- Adds filter to change sort order of years in publication_year facet 
## [1.1.8] - 2018-01-30
### Changed
- Corrects CSS on Project archive page
## [1.1.7] - 2018-01-30
### Changed
- Corrects Staff CPT slug to remove "blog/" front
## [1.1.6] - 2018-01-30
### Changed
- Moved ACF JSON path code to bootstrap - wasn't being loaded in general
## [1.1.5] - 2018-01-30
### Changed
- Changed plugin description
## [1.1.4] - 2018-01-30
### Added
- Styling to allow more items per line on Project archive, style project item
description and remove button.

## [1.1.2] - 2018-01-22
### Changed
- Altered Project CPT creator class to support featured image, author, revisons, excerpt
## [1.1.1] - 2018-01-22
### Changed
- Project CPT definition to allow support featured image
## [1.1.0] - 2018-01-22
### Added
- New branch project-grid for project category archive
- JS scripts for Isoptope masonry scripts
### Changed
- Version bump
## [1.0.4] - 2018-01-18
### Added
-Initial commit
### Changed
- N/A

