jQuery(document).ready(function($){
    
    // we're going to hijack the clicking of the translation link
    // look for the value of the facet language dropdown
    // and if it's anything other than blank, use ajax to slugify, 
    // shove it on the end of the URL
    // and redirect.
    
    //$('.facetwp-facet-test_language ')
    
    $(document).on('click', '.project-list-template .fwpl-item a', function(e){
        e.preventDefault();
        // where are we going?
        var destinationURL = $(this).attr('href');
        
        // use the text of the select because languages with foreign chars are encoded...
        var currentLang = $('.facetwp-facet-test_language select option:selected').text();
        console.log(destinationURL);
        if( currentLang.length == 0 || 'Any language' === currentLang ) {
            // no current language, so just do the redirection
            window.location.href = destinationURL;
        } else {
            var input = {
                action : 'cscs_slugify_ajax',
                lang : currentLang
            }

            $.post( cscs_ajax.ajaxurl, input, function(data) {
                if( data.error !== false ) {
                    // there's an error, so just redirect without lang string:
                    window.location.href = destinationURL;
                } else {
                    // everything worked, so tag the correct destintion on the end:
                    finalDestination = destinationURL + '#' + data.slug;
                    window.location.href = finalDestination;
                }
            });     
        } 
    });
    
    // add slick slider for home page
    $('.slick-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
        adaptiveHeight: true,
        pauseOnHover: false,
        fade: true,
        speed: 500,
    });
    
    
    //set the height of the slideshow backgrounder based on content
    var slideElement = $('.front-page-1');
    if( slideElement.length > 0 ) {
        var slideHeight = slideElement.height();
        $('.single-slide').height( slideHeight );
    }
    
});