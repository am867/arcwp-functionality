<?php
/*
 * Template Name:FAQ Page
 * n.b. ACF fields will appear on the page with slug frequently-asked-questions
*/
//* Load faq.js, faq.css
wp_enqueue_script( 'faq-js',  plugins_url( '/',dirname(__FILE__ ))  . 'js/faq.js', array( 'jquery' ), '1', true );
wp_enqueue_style ( 'faq-css' , plugins_url( '/',dirname(__FILE__ ))  . '/css/faq.css', '', '1', 'all' );

//* Render the ACF FAQ repeater rows
add_action( 'genesis_entry_content', 'wpb_faq_repeater_page', 10 );//Position FAQs after post content

function wpb_faq_repeater_page () {

	if( have_rows('faq_page') ):
		while ( have_rows('faq_page') ) : the_row();
			echo '<h3 class="faq-title">'.get_sub_field('faq_category').'</h3>';
// check if the repeater field has rows of data
			if( have_rows('faqs') ):
				// loop through the rows of data
				while ( have_rows('faqs') ) : the_row();

					// display a sub field value
					echo'<div class="faq_container"> 
							<div class="faq">
							<div class="faq_question"> <span class="question">' . get_sub_field('faq_question') . '</span><button class="accordion-button-icon fa fa-plus"></button></div>
									<div class="faq_answer_container">
										<div class="faq_answer"><span>' . get_sub_field('faq_answer') . '</span></div>
									</div>
							</div>
			            </div>';

				endwhile;

			else :

				// no rows found

			endif;
	    endwhile;
	endif;

}

genesis();