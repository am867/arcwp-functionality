<?php
/*
Template Name: Custom Taxonomy Archive Page
 * @package      Staff CPT and shortcode
 * @since        0.1.0
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
*/

/*
 * Replace the default page title with the Research Project name
 */

// Make sure the taxonomy archives are sorted alphabetically on name


if ( !class_exists( 'staff_tax_archive' ) ) :

    class  staff_tax_archive
    {
        // vars
        protected $queried_object;
        protected $staffTaxonomy;
        protected $term_id;
        protected $staffTerm;

        public function __construct(){$this->setup_actions();}

        private function setup_actions(){
            // Redirect Page if URL specified on Term edit page ...
            add_action ('get_header', array($this,'get_staff_tax_redirect'),0);
            // Force full-page layout
            add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
            // Remove staff meta data from listing
            remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
            remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
            // Remove the feautured image from the list items
            add_filter( 'genesis_pre_get_option_content_archive_thumbnail', array($this,'cscs_hide_post_image' ));
            // Use the current Taxonomy term as the page title
            add_filter ('be_page_title',array($this,'cpt_page_title'),0);
            // Allow shortcode in Term Archive Intro Text
            add_filter( 'genesis_term_intro_text_output', 'do_shortcode' );
            // Replace the standard loop with our custom loop
            remove_action( 'genesis_loop', 'genesis_do_loop' );
            add_action( 'genesis_loop', array($this,'do_staff_loop' ));

        }
        public function cscs_hide_post_image() {return '0';}


        public function cpt_page_title(){

            $staffTaxonomy=get_query_var('taxonomy');
            $staffTerm= get_term_by('slug',get_query_var('term'),$staffTaxonomy);


            switch ($staffTaxonomy) {

               case research_project:

                    $title=($staffTerm->name . "Research&nbsp;Project&nbsp;Staff");

                    return $title;

                    break;

               case staff_affiliation:

                   $title=($staffTerm->name . "&nbsp;Affiliation&nbsp;Staff");

                   return $title;

                   break;

                case staff_role:

                    $title=($staffTerm->name . "&nbsp;Role&nbsp;Staff       ");

                    return $title;

                    break;

                default:

                    return  $staffTerm;

                    break;
            }
        }

        /*
         * Redirect to a new URL if defined in taxonomy term edit page
         * Note that redirections to external domains are not allowed unless
         * specified in the relevant section of inc/general.php
         */

        public function get_staff_tax_redirect(){

            $queried_object = get_queried_object();

            // load Home Page URLfor this taxonomy term (term object)
            $tax_redirect = esc_url(get_field('staff_tax_redirect', $queried_object));

            if (!empty($tax_redirect)) {

                wp_safe_redirect($tax_redirect);

                exit;
            }
        }

        /*
         * Replace the Genesis loop
        */
        public function do_staff_loop(){
	        global $wp_query;
	        global $isFacetWP;

	        $isFacetWP =  is_plugin_active( 'facetwp/index.php' );
            // start rendering

	        if ( $isFacetWP )  {
           // echo '<h2>'.facetwp_display( 'facet', 'staff_az' ).'</h2>';
            //echo '<div style="display: inline-block;font-weight: bold">Filter on Role&nbsp;</div><div style="display: inline-block;">'.facetwp_display('facet', 'staff_role' ). '</div>';

	        }?>
            <h3>Staff</h3>
            <div class="facetwp-template"  >
                <div class="one-third first header"><h4>Name</h4></div>
                <div class="one-third header"><h4>Email</h4></div>
                <div class="one-third header"><h4>Job Title</h4></div>

                <?php  while ( have_posts() ) : the_post();?>
                    <div class="<?php echo 'facet-row '. ($wp_query->current_post%2 == 0?'odd':'even'); ?>">

                    <?php $displayLink=get_post_meta(get_the_ID(),'staff_link',true);?>

		            <?php if ($displayLink) { ?>

                   <div class="one-third first name"><strong><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></strong></div><?php }else{ ?>
                    <div class="one-third first name"><strong><strong></strong><?php the_title(); ?></strong></div>  <?php } ?>

                   <?php
                   $staffEmail = get_post_meta( get_the_ID(), 'staff_email', true ) ;
                   if ($staffEmail){echo "<div class=\"one-third email\"><a href=\"mailto:".$staffEmail ."\">". $staffEmail."</a></div>";}
                   else {echo "<div class=\"one-fourth email\">&nbsp;</div>";}?>
                   <div class="one-third role" style="text-align: left;"><?php
	                   $staffTitle = get_post_meta( get_the_ID(), 'staff_job_title', true ) ;
                   if ($staffTitle) {
	                            echo "&nbsp;". $staffTitle;
                        } else {echo "&nbsp;";}
                   ?>

                    </div>


                        <div class="clearfix"></div></div>
                <?php endwhile; // end the custom loop
                if ( !$isFacetWP ) { do_action( 'genesis_after_endwhile' );}
	        if ( $isFacetWP )  {echo '<br/><p>'.do_shortcode( '[facetwp pager="true"]' ).'</p>';
	        }

                wp_reset_postdata(); // always reset post data after a custom query


                ?>
            </div> <?php
        }
    }

endif;

new staff_tax_archive();

genesis();