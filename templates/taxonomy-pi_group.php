<?php
/*
Template Name: PI Group Taxonomy Archive Page
 * @package      Staff CPT and shortcode
 * @since        0.1.6
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
*/

/*
 * Replace the default page title with the PI Group name
 */


if ( !class_exists( 'pi_tax_archive' ) ) :


class  pi_tax_archive
    {
        private $queried_object;

        public function __construct()
        {
            $this->setup_actions();
        }

        private function setup_actions()
        {
            //$queried_object= get_queried_object();

            // Redirect Page if URL specified on Term edit page ...

            add_action ('get_header', array($this,'get_staff_tax_redirect'),0);

            // .. Otherwise:

            // Force full-page layout

            add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

            // Remove staff meta data from listing

            remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

            remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

            // Use the current Taxonomy term as the page title

            add_filter ('be_page_title',array($this,'cpt_page_title'),0);

            // Add in a default description

            remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );



        }

        public function cpt_page_title(){

            $title= ucfirst(get_query_var( 'research_project' ))." Project";

            return $title;

        }

        /*
         * Redirect to a new URL if defined in taxonomy term edit page
         * Note that redirections to external domains are not allowed unless
         * specified in the relevant section of inc/general.php
         */

        public function get_staff_tax_redirect(){
            // vars
            $queried_object = get_queried_object();
            //$taxonomy = $queried_object->taxonomy;
            //$term_id = $queried_object->term_id;

            // load Home Page URLfor this taxonomy term (term object)
            $staff_tax_redirect = esc_url(get_field('staff_tax_redirect', $queried_object));

            if (!empty($staff_tax_redirect)) {

                wp_safe_redirect($staff_tax_redirect);

                exit;
            }
        }
    }


endif;

new pi_tax_archive();

genesis();