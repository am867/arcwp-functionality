<?php
/*
 * Template Name: Past projects
 */


/**
 * Past projects template
 * Author: dt216
 * Required FacetWP facet called Project and template called Projects
 * @Since 1.5.0
 */
// TODO: remove Isotope JS and associated CSS, old archive project template
//* Add project body class

add_filter( 'body_class', 'altitude_add_project_body_class' );
function altitude_add_project_body_class( $classes ) {
	$classes[] = 'altitude-pro-project';
	return $classes;
}

//* Force full width content layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );


//* Remove post info and meta functions
remove_action( 'genesis_entry_header', 'genesis_post_info', 5 );
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

// Remove the default loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

// Add the Search facets below archive description
function cscs_add_facets_innit() {


    echo the_content();

    // create a new loop to go through projects:

    $project_args = array(
        'post_type' => 'project',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => array(
            array (
                'key' => 'project_staus',
                'value' => 'Complete'
            )
        )
    );

    $projects_loop = new WP_Query( $project_args );
    $output = '';
    $post = '';

    if( $projects_loop->have_posts() ) :
        $output .= "<div class='project-list-template'><div class='facetwp-template'><div class='fwpl-layout el-d7ulp8 project-item'>";
        while( $projects_loop->have_posts() ) : $projects_loop->the_post();

        $imageLabel = get_field('project_recruitment');

        $output .= "<div class='fwpl-result'><div class='fwpl-row'><div class='fwpl-col'>";
            // image:
            $output .= "<div class='fwpl-row'><div class='fwpl-col'><div class='fwpl-item'>";
                $output .= "<a href='" . get_the_permalink() . "'>";
                if( $imageLabel && 'yes' === $imageLabel ) :
                    $output .= "<span class='captions'>Take part</span>";
                endif;

                $output .= get_the_post_thumbnail( get_the_ID(), 'project-list' );
                $output .= "</a>";
            $output .= "</div></div></div>";
            // title:
            $output .= "<div class='fwpl-row'><div class='fwpl-col'><div class='fwpl-item  el-rrbhlh'>";
                $output .= "<a href='" . get_the_permalink() . "' target=''>" . get_the_title() . "</a>";
            $output .= "</div></div></div>";
            // programme:
            // @todo: conditional?
            $output .= "<div class='fwpl-row'><div class='fwpl-col'><div class='fwpl-item'>";
                $output .= "<strong>Programme:</strong> &nbsp;";
                // get programme terms:
                $project_terms = get_the_terms( get_the_ID(), 'programme' );
                $terms_array = array();
                foreach( $project_terms as $term ) :
                    $terms_array[] = "<span class='fwpl-term fwpl-term-{$term->slug} fwpl-tax-programme'>{$term->name}</span>";
                endforeach;
                $terms_string = implode( ', ', $terms_array ); // imploding an array handles trailing commas better than foreach!
                $output .= $terms_string;
            $output .= "</div></div></div>";

            // excerpt:
            $output .= "<div class='fwpl-row'><div class='fwpl-col project-grid-excerpt'><div class='fwpl-item'>";
                $output .= "<p>" . get_the_excerpt() . "</p>";
            $output .= "</div></div></div>";

            $output .= "</div></div>";

            // button:
            $output .= "<div class='fwpl-row'><div class='fwpl-col'><div class='fwpl-item'>";
                $output .= "<a href='" . get_the_permalink() . "' target=''><button>Find out more</button></a>";
            $output .= "</div></div></div>";


        $output .= "</div>";


        endwhile;
        $output .= "</div></div></div>";
        wp_reset_postdata();
    endif;

        $output .= "<style>
                .fwpl-layout {
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr;
                    grid-gap: 100px;
                }
                .fwpl-row {
                    display: grid;
                }
                .fwpl-row.el-r1a8ar {
                    grid-template-columns: 1fr;
                }
                .fwpl-row.el-nkwa2g {
                    grid-template-columns: 1fr;
                }
                .fwpl-row.el-z1voa3 {
                    grid-template-columns: 1fr;
                }
                .fwpl-item.el-rrbhlh,
                .fwpl-item.el-rrbhlh a {
                    font-weight: bold;
                }
                .fwpl-row.el-zz0j34 {
                    grid-template-columns: 1fr;
                }
                .fwpl-item.el-imzqia,
                .fwpl-item.el-imzqia a {
                    text-align: left;
                }
                .fwpl-row.el-fl5i21 {
                    grid-template-columns: 1fr;
                }
                .fwpl-row.el-039oq {
                    grid-template-columns: 1fr;
                }

                @media (max-width: 480px) {
                    .fwpl-layout {
                        grid-template-columns: 1fr;
                    }
                }
                </style>";

    echo $output;
}



add_action('genesis_before_loop','cscs_add_facets_innit');

genesis();