<?php
/*
Template Name: Single Staff Page
 * @package      Staff CPT and shortcode
 * @since        0.1.3
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
 */

// Removes default content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );


// create function for sorting:
// Comparator function used for comparator 
// scores of two object/students 
function compare_years( $object1, $object2 ) { 
    return $object1->year < $object2->year; 
} 

/*
 * Display the custom content (ACF field values)
 */
add_action( 'genesis_entry_content', 'child_do_content' );
function child_do_content()
{

    $image = get_post_meta(get_the_ID(), 'staff_photo', true);
    $imageURL= wp_get_attachment_image_src($image);
    $imageAlt=get_post_meta($image, '_wp_attachment_image_alt', true);
    $staffJobTitle = get_post_meta(get_the_ID(), 'staff_job_title', true);
    
    
    $is_collaborator = has_term( 'collaborators', 'staff_role', get_the_ID() );

    //echo '<div style="float:right;padding:0 0 0 20px"><img src="' . $imageURL[0]. '" alt="' . $imageAlt . '" /></div>';

    // Display main content
	echo '<div class="staff-photo">'.wp_get_attachment_image( $image, 'medium' ).'</div>';
    if ($staffJobTitle) {
	    echo '<h3 class="staff-job-title">' . $staffJobTitle . '</h3>';
    }
	echo the_terms( $post->ID, 'staff_affiliation', '<p><strong>Affiliations: ', ', ', '</strong></p>' );


    $career=get_post_meta(get_the_ID(),'staff_main_content',true);
    if (!empty($career)) {
       // echo '<div>'.wpautop( get_post_meta(get_the_ID(),'staff_main_content',true)).'</div>';
	   //echo '<div style="float:right;padding:0 0 0 20px">'.wp_get_attachment_image( $image, 'medium' ).'</div>';
	    echo the_field('staff_main_content');

	    echo '<div>&nbsp;</div>';
    }


	//List associated projects
	wp_reset_postdata();
	$hideProjects=get_post_meta(get_the_ID(),'staff_hide_projects',true);
    if(!$hideProjects) {
	    $staffProjects = get_field( 'project_staff' ); // todo: doh! this should have been staff_project
	    if ( $staffProjects ):
		    echo '<h3>Projects </h3><ul>';
		    foreach ( $staffProjects as $post ):
			    setup_postdata( $post ); ?>
                <li>
                <a href="<?php the_permalink( $post->ID ); ?>">
				    <?php echo get_the_title( $post->ID ); ?>
                </a>&nbsp;</li><?php
		    endforeach;
		    ?></ul><?php
	    endif;
    }
    /*
     * Render publications
     */

	// TODO: replace ACF field function with WP get_post_meta
	//$publications = get_post_meta( get_the_ID(), 'staff_publications', true );

	$publications=get_field('staff_publications');

	global $post;

	if ( $publications && !$is_collaborator ):
		echo '<h3>Selected Publications</h3><ul>';
    
        // sort by date:
    
        
    
        foreach( $publications as $pub_key => $publication ) :
            $publication_year = get_post_meta( $publication->ID, 'pub_year', false );
//            var_dump($publication_year[0]);
            $publications[$pub_key]->year = $publication_year[0];
        endforeach;
        
        usort( $publications, 'compare_years' );
            
        // @todo: set the correct links?
    
		foreach( $publications as $post ):
			$pubAmazon='lib.autismresearchcentre.com/papers/';
			setup_postdata($post); ?>
			<li>                
				<?php $pubAmazon=$pubAmazon.get_field( 'legacy_file_link') ?>
				<a href="http://<?php echo $pubAmazon; ?>"><?php echo rtrim( get_the_title( $post->ID ), ' ' ); ?></a>,
				<?php
				//echo the_terms( $post->ID, 'publication_journal', '', '', '' );
				$journals = wp_get_object_terms($post->ID,'publication_journal');
				foreach ($journals as $journal){
					echo $journal->name;
				}
				?>
				<?php echo the_field( 'pub_volume' ); ?>
				(<?php echo the_field( 'pub_year' ); ?>),
				<?php echo the_field( 'pub_authors' ); ?>

			</li>
		<?php endforeach; ?>
		</ul>
 <?php
        endif;
	$url = site_url();

	echo '<br/><br/><strong><a href="'.$url.'/staff/">Return to staff directory</a></strong>';


}
// Remove post meta in footer
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
genesis();