<?php
/*
Template Name: Staff Archive Page
 * @package      Staff CPT and shortcode
 * @since        0.1.5
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
*/
/*
 * Change page title via Genesis Staff > Archive Settings
 */

remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop

//

genesis();