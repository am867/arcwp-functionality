<?php
/**
 * This file adds the custom project post type archive template to the Altitude Pro Theme.
 *
 */

//* Add portfolio body class
add_filter( 'body_class', 'altitude_add_project_body_class' );
function altitude_add_project_body_class( $classes ) {
	$classes[] = 'altitude-pro-project';
	return $classes;
}

//* Load Isotope
wp_enqueue_script( 'isotope',  plugins_url( '/',dirname(__FILE__ ))  . 'js/jquery.isotope.min.js', array( 'jquery' ), '1.5.26', true );
wp_enqueue_script( 'isotope_init',  plugins_url( '/',dirname(__FILE__ ))  . 'js/isotope_init.js', array( 'isotope' ), '1.0.0', true );

//* Force full width content layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Display Portfolio Categories Filter
add_action( 'genesis_before_loop', 'sk_isotope_filter' );
function sk_isotope_filter() {

	if ( is_post_type_archive( 'project' ) )
    $terms = get_terms( 'programme',array(
	    'orderby' => 'name',
	    'order'   => 'ASC',
	    'exclude' => array(950),
    )  );

	$count = count( $terms ); $i=0;
	if ( $count > 0 ) { ?>
		<ul id="project-cats" class="filter clearfix">
			<li><a href="#" class="active" data-filter="*"><span><?php _e('All', 'genesis'); ?></span></a></li>
			<?php foreach ( $terms as $term ) : ?>
				<li><a href="#" data-filter=".programme-<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
			<?php endforeach; ?>
		</ul><!-- /project-cats -->
	<?php }
}

//* Display 'back to project' link and Title on Project Category archive pages
/*add_action( 'genesis_before_loop', 'sk_taxonomy_page_additions' );
function sk_taxonomy_page_additions() {

	if ( is_tax( 'programme_taxonomy' ) ) {

		echo '<a href="' . get_bloginfo( 'url' ) . '/projects/">&laquo; Back to Projects</a>';
		global $wp_query;

		$term = $wp_query->get_queried_object();
		echo '<h2 class="taxonomy-title">' . $term->name . '</h2>';
	}
}*/

//* Wrap Project items in a custom div - opening
add_action('genesis_before_loop', 'project_content_opening_div' );
function project_content_opening_div() {
	echo '<div class="project-wrap"><div class="project-content">';
}

//* Remove the breadcrumb navigation
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove post info function
remove_action( 'genesis_entry_header', 'genesis_post_info', 5 );

//* Force Excerpts
add_filter( 'genesis_pre_get_option_content_archive', 'sk_show_excerpts' );
function sk_show_excerpts() {
	return 'excerpts';
}

//* Modify the length of post excerpts
add_filter( 'excerpt_length', 'sp_excerpt_length' );
function sp_excerpt_length( $length ) {
	return 10; // pull first 10 words
}

//* Modify the Excerpt read more link
add_filter( 'excerpt_more', 'new_excerpt_more' );
function new_excerpt_more( $more ) {
	return '... <a href="' . get_permalink() . '">More</a>';
}

//* Remove the post image
remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

//* Remove post meta function
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

//* Do not show Featured image if set in Theme Settings > Content Archives
add_filter( 'genesis_pre_get_option_content_archive_thumbnail', '__return_false' );

//* Add featured image above or below .entry-header in each project item in the listing
add_action( 'genesis_entry_header', 'altitude_portfolio_grid', 4 );
//add_action( 'genesis_after_entry_content', 'altitude_portfolio_grid', 4 );
function altitude_portfolio_grid() {
	if ( $image = genesis_get_image( 'format=url&size=large' ) ) {
		printf( '<div class="project-featured-image"><a href="%s" rel="bookmark"><img src="%s" alt="%s" /></a></div>', get_permalink(), $image, the_title_attribute( 'echo=0' ) );
    } else {
	    //* Add a default to the project listing in case no r *//
	    $default_image = plugins_url('images/default-project.jpg',dirname(__FILE__) ) ;
		printf( '<div class="project-featured-image"><a href="%s" rel="bookmark"><img src="'.$default_image.'" alt="%s" /></a></div>', get_permalink(), "", the_title_attribute( 'echo=0' ) );
	}
}

//* Wrap .entry-header and .entry-content in a custom div - opening
add_action( 'genesis_entry_header', 'sk_opening_div', 4 );
function sk_opening_div() {
	echo '<div class="entry-content-wrap">';
}

//* Wrap .entry-header and .entry-content in a custom div - closing
add_action( 'genesis_entry_footer', 'sk_closing_div' );
function sk_closing_div() {
	echo '</div>';
}

// add category names in post class
add_filter( 'post_class', 'project_category_class' );
function project_category_class( $classes ) {

	$terms = get_the_terms( get_the_ID(), 'programme' );
	if( $terms ) foreach ( $terms as $term )
		$classes[] = $term->slug;

	return $classes;

}

//* Wrap Project items in a custom div - closing
add_action('genesis_after_loop', 'project_content_closing_div' );
function project_content_closing_div() {

	echo "</div></div>";

}

genesis();