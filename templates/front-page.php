<?php
/**
 * Altitude Pro.
 *
 * This file adds the front page to the Altitude Pro Theme.
 *
 * @package Altitude
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://my.studiopress.com/themes/altitude/
 */

add_action( 'genesis_meta', 'altitude_front_page_genesis_meta' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 * @since 1.0.0
 */
function altitude_front_page_genesis_meta() {

	if ( is_active_sidebar( 'front-page-1' ) || is_active_sidebar( 'front-page-2' ) || is_active_sidebar( 'front-page-3' ) || is_active_sidebar( 'front-page-4' ) || is_active_sidebar( 'front-page-5' ) || is_active_sidebar( 'front-page-6' ) || is_active_sidebar( 'front-page-7' ) ) {

		// Enqueue scripts.
		add_action( 'wp_enqueue_scripts', 'altitude_enqueue_altitude_script' );

		// Add front-page body class.
		add_filter( 'body_class', 'altitude_body_class' );

		// Force full width content layout.
		add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

		// Remove breadcrumbs.
		remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

		// Remove the default Genesis loop.
		remove_action( 'genesis_loop', 'genesis_do_loop' );

		// Add homepage widgets.
		add_action( 'genesis_loop', 'altitude_front_page_widgets' );

		// Add featured-section body class.
		if ( is_active_sidebar( 'front-page-1' ) ) {

			// Add image-section-start body class.
			add_filter( 'body_class', 'altitude_featured_body_class' );

		}

	}

}

// Define front page scripts.
function altitude_enqueue_altitude_script() {
	wp_enqueue_script( 'altitude-script', get_stylesheet_directory_uri() . '/js/home.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
}

// Define front-page body class.
function altitude_body_class( $classes ) {

	$classes[] = 'front-page';

	return $classes;

}

// Define featured-section body class.
function altitude_featured_body_class( $classes ) {

	$classes[] = 'featured-section';

	return $classes;

}


// Add slider for home page

function arc_homepage_slideshow () {
    
    // each slide is just a featured image and title, saved as a CPT
    // so we just loop through
    
    $before_output = $after_output = '';
    
    $args = array(
        'post_type' => 'slider',
        'posts_per_page' => '-1',
        'order' => 'ASC'
    );
    
    $loop = new WP_Query( $args );
    
    // start the wrapper
    
    echo "<div class='slideshow-wrap'>";
    
    if( $loop->have_posts() ) : 
        echo "<div class='slick-slider'>"; // change this to add to string when we return not echo
        while( $loop->have_posts() ) :
            $loop->the_post();
            $background = get_the_post_thumbnail_url( $post, 'full' );
            $before_output .= "<div class='single-slide' style='background-image: url( $background );'>";
            echo $before_output;
            echo "<div class='tint'></div>";
            $after_output .= "</div>";
            echo $after_output;


            wp_reset_postdata();
        endwhile; 
        echo "</div>";
    
    
        // now do the content which sits over the top:
    
        // for now we'll use the genesis widget approach although the way it just echoes is far from ideal
        // in future we can replace this to use the_content etc for different content per slide
        // if we do this we can return from this function rather than echo

        genesis_widget_area( 'front-page-1', array(
            'before' => '<div id="front-page-1" class="front-page-1" tabindex="-1"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-1' ) . '"><div class="wrap">',
            'after'  => '</div><a class="morelink" href="#front-page-3"><i class="fa fa-chevron-down"></i></a></div></div></div>',
        ) );
    
    
    endif;
    
    echo "</div>"; // end wrap
    
    
}



// Add markup for front page widgets.
function altitude_front_page_widgets() {

	echo '<h2 class="screen-reader-text">' . __( 'Main Content', 'altitude-pro' ) . '</h2>';
    
    
    arc_homepage_slideshow();
	

	genesis_widget_area( 'front-page-2', array(
		'before' => '<div id="front-page-2" class="front-page-2" tabindex="-1"><div class="solid-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-2' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-3', array(
		'before' => '<div id="front-page-3" class="front-page-3" tabindex="-1"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-3' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-4', array(
		'before' => '<div id="front-page-4" class="front-page-4" tabindex="-1"><div class="solid-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-4' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-5', array(
		'before' => '<div id="front-page-5" class="front-page-5" tabindex="-1"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-5' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-6', array(
		'before' => '<div id="front-page-6" class="front-page-6" tabindex="-1"><div class="solid-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-6' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-7', array(
		'before' => '<div id="front-page-7" class="front-page-7" tabindex="-1"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-7' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

}

// Run the Genesis loop.
genesis();
