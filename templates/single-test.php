<?php
/*
Template Name: Single Test Page
 * @package      Test CPT and shortcode
 * @since        0.1.3
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
 */

// Force content-sidebar layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Removes default content

remove_action( 'genesis_entry_content', 'genesis_do_post_content' );


// Reformat the page title post-style to page-style

function single_profile_title(){
    $title = get_the_title();
    return $title;
}
add_filter ('be_page_title','single_profile_title');

// Adds your custom page code/content
// Add title back in page-style

add_action( 'genesis_entry_content', 'child_do_content' );

// Remove Post strapline i.e. date post created, author

add_filter( 'genesis_post_info', 'remove_single_cpt_post_info' );

function remove_single_cpt_post_info($post_info) {
    if ( is_singular('test') ) :
        $post_info = '[post_edit]';
        return $post_info;
    endif;
}
// Remove post meta
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

// Display the custom content (ACF field values)

function child_do_content()
{

    // $image = get_post_meta(get_the_ID(), 'staff_photo', true);
    // $imageURL= wp_get_attachment_image_src($image);
    // $imageAlt=get_post_meta($image, '_wp_attachment_image_alt', true);

    $description = get_post_meta(get_the_ID(),'test_description',true);
    if (!empty($description)) {
       
	    echo the_field('test_description');

	    echo '<div>&nbsp;</div>';
    }

	$references = get_post_meta(get_the_ID(),'test_references',true);
    if (!empty($references)) {
	   
		echo '<h3>References</h3>';
	    echo the_field('test_references');

	    echo '<div>&nbsp;</div>';
    }

	// check to make sure there are test downloads
	if( have_rows('test_downloads') ):
		// render the table header

		echo "<div class=\"test-downloads\">";
			echo "<div class=\"test-headers\">";
				echo "<div class=\"one-fourth first\"><h4>Language</h4></div>";
				echo "<div class=\"one-fourth \"><h4>Test Download</h4></div>";
				echo "<div class=\"one-half\"><h4>Contributors/Notes</h4></div>";
				echo "<div class=\"clearfix\"></div>";
			echo "</div>";


		// loop through the rows of data

		while ( have_rows('test_downloads') ) : the_row();

			// Add odd/even class to each row

			echo '<span id="' . sanitize_title(get_sub_field('test_download_language')) . '" class="anchor"></span><div class="test-download-row '. ( get_row_index()%2 == 0?'even':'odd').'">';
    
    
            // add title (not all downloads are straight up translations)
            echo "<div class='download-title'><h6>" . get_sub_field( 'test_title' ) . "</h6></div>";       
    
			// display language field value
			echo "<div class=\"one-fourth first download-language\"><span>" . get_sub_field('test_download_language') ."</span></div>";

			// display download link
			//$test_download = get_sub_field('test_file_download');
			//$download_url = $test_download['url'];

			//TODO: map test_file_download to an S3 folder with S3 Offloader plugin

			$download_url= get_sub_field('test_link_download');
    
            if( '' === $download_url || empty($download_url) ) :
                $download_url = get_sub_field( 'test_upload' );
            else :
                $download_url = 'https://docs.autismresearchcentre.com/tests/' . $download_url;
            endif;

			echo "<div class=\"one-fourth download-url\"><span><a href=\"" . $download_url ."\">Download</a></span></div>";

			// display download credits
			echo "<div class=\"one-half download-credits\">" . get_sub_field('test_download_references') ."</div>";

			echo "</div>";

			echo "<div class=\"clearfix\"></div>";

		endwhile;
		echo "</div>";
	else :

		// no rows found

	endif;
    
}

genesis();