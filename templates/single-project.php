<?php
/*
Template Name: Single Profile Page
 * @package      Staff CPT and shortcode
 * @since        0.1.3
 * @link
 * @author       Dave Tasker <dt216@cam.ac.uk>
 */
// Force content-sidebar layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Removes default content
//remove_action( 'genesis_entry_content', 'genesis_do_post_content' );


// Adds your custom page code/content
// Add title back in page-style

add_action( 'genesis_entry_content', 'child_do_content' );

// Remove Post strapline i.e. date post created, author

add_filter( 'genesis_post_info', 'remove_single_cpt_post_info' );

function remove_single_cpt_post_info($post_info) {
    if ( is_singular('project') ) :
        $post_info = '[post_edit]';
        return $post_info;
    endif;
}
// Remove post meta
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

// Display the custom content (ACF field values)

function child_do_content()
{

    echo "<div class='project-content-wrap'>";
    echo '<div class="project-content">';

    /* Show the recruitment details if applicable*/
    $projectRecruitment = get_post_meta(get_the_ID(),'project_recruitment',true);
	$recruitmentDetails = get_post_meta(get_the_ID(),'project_recruitment_details',true);

    if ($projectRecruitment === "yes" && $recruitmentDetails ){

        $recruitment = "<div class='recruitment-details'>";
	    $recruitment .= "<h2>Recruitment Details</h2>";
        $recruitment .= apply_filters( 'the_content', $recruitmentDetails );
        $recruitment .= "</div>";

        echo $recruitment;

    }
    // Display main
    $projectContent=get_post_meta(get_the_ID(),'project_content',true);
    if ($projectContent):


        $content .= apply_filters( 'the_content', get_post_meta(get_the_ID(), 'project_content', true ) );

        echo $content;

    endif;


    echo "</div><br style='clear:both;'></div>";

    /* Generate the selected publications list */
	$projectStaff = get_post_meta(get_the_ID(),'project_staff',true);
	// Use the ACF field key otherwise imported posts with empty project_content will error-out
	if (is_array($projectStaff)):
		global $post;
		echo "<h2>Staff Members</h2><ul>";
		foreach ($projectStaff as $post ):
			setup_postdata( $post ); ?>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            <br/>
		<?php endforeach;
		?></ul><?php
	endif;
	// now do the publications
	wp_reset_postdata();
	$publications=get_field('project_publications');

	if (is_array($publications)):
		echo '<h2>Selected Publications</h2>';
		?> <ul><?php
		foreach( $publications as $post ):
			$pubAmazon='lib.autismresearchcentre.com/papers/';
			setup_postdata($post); ?>
            <li>
				<?php $pubAmazon=$pubAmazon.get_field( 'legacy_file_link') ?>
                <a href="http://<?php echo $pubAmazon; ?>"><?php echo get_the_title( $post->ID ); ?></a>,
				<?php
				//echo the_terms( $post->ID, 'publication_journal', '', '', '' );
				$journals = wp_get_object_terms($post->ID,'publication_journal');
				foreach ($journals as $journal){
					echo $journal->name;
				}
				?>
				<?php echo the_field('pub_volume'); ?>
                (<?php echo the_field( 'pub_year' ); ?>),
				<?php echo the_field( 'pub_authors' ); ?>

            </li>
		<?php endforeach; ?>
    </ul>
	<?php
	endif;

	$archive_link = get_post_type_archive_link( "project" );
	echo "<p><a class='button btn btn-primary' href='$archive_link'>Back to projects</a></p>";

}
/*
 * Enable this is Raven only required
 */
// if (!is_user_logged_in()) { auth_redirect(); }

genesis();