<?php

/**
  Template Name: Tests Archive Page

  @package Test CPT
  @since 0.1.5
 */
//* Add test archive body class
add_filter( 'body_class', 'cscs_add_test_post_body_class' );
function cscs_add_test_post_body_class( $classes ) {
	$classes[] = 'arc-test-archive';
	return $classes;
}
// Force full width layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Remove post info and meta functions
remove_action( 'genesis_entry_header', 'genesis_post_info', 5 );
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop

add_action( 'genesis_loop', 'cscs_tests_facet_template' );
// Add the Search facets below archive description
function cscs_tests_facet_template() {

    // template from other filters:
    //echo '<div>'; - can use .one-half etc to set width
    //    echo 'Programme:';
    //    echo '<div class="filter-programme" >'.facetwp_display( 'facet', 'project_programme' ).'</div>&nbsp;';
    //echo '</div>';

    echo "<div class='first one-half'>";
        echo 'Search:';
        echo '<div class="project-filter filter-language" >'.facetwp_display( 'facet', 'tests_search' ).'</div>';
    echo "</div>";
    echo "<div class='one-half'>";
        echo 'Choose a translation:';
        echo '<div class="project-filter filter-language" >'.facetwp_display( 'facet', 'test_language' ).'</div>';
    echo "</div>";

	// echo '<div class="clearfix"></div>';

	// echo '<div class="project-filter filter-label" >Active Filters:&nbsp;</div>';

	// echo '<div class="project-filter filter-selection" >' .facetwp_display( 'selections' ).'</div>';

	// echo '<div class="clearfix"></div>';

    echo '<button onclick="FWP.reset()">Reset</button>';

	echo '<div class="clearfix"></div>';

	// display the project list
	echo '<div class="project-list-template">'.facetwp_display( 'template', 'tests' ).'</div>';
	echo '<br/>';
}


genesis();