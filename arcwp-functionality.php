<?php
/**
 * Plugin Name: ARCWP Functionality
 * Plugin URI:
 * Description: ARC website customisations: general fn, CSS overrides and staff, publication and project CPTs
 * Author:      Dave Tasker <dt216@cam.ac.uk>
 * Text Domain: cscs_ns
 * Version:     1.11.1
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2, as published by the
 * Free Software Foundation.  You may NOT assume that you can use any other
 * version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 *
 *
 */

/**
 * Check that WordPress is loaded and plugin dependencies are active
 * TODO: add missing dependencies, upload CSCS CPT widget plugin to repo.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
register_activation_hook( __FILE__, 'child_plugin_activate' );
function child_plugin_activate(){
    if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' )or
            ! is_plugin_active( 'arc-style/arc-style.php' ) or
            ! is_plugin_active( 'facetwp/index.php' ) or
            ! is_plugin_active( 'amazon-s3-and-cloudfront/wordpress-s3.php' ) or
            ! is_plugin_active( 'acf-cpt-options-pages/cpt-acf.php' ) and
            current_user_can( 'activate_plugins' ) ) {

        // Stop activation redirect and show error
        wp_die('Sorry, but the following plugins must be installed and active:
                <ul>
                <li><a href="https://bitbucket.org/cscswebservices/arc-style">ARC Style</a></li>
                <li><a href="https://facetwp.com/">FacetWP</a></li>
                <li><a href="https://www.advancedcustomfields.com/pro/">Advanced Custom Fields Pro</a> </li>
                <li><a href="https://wordpress.org/plugins/amazon-s3-and-cloudfront/">Media Offloader Lite</a></li>
                <li><a href="https://wordpress.org/plugins/acf-cpt-options-pages//">Advanced Custom Fields : CPT Options Pages</a></li>
                </ul>
                <a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
    }
}

/**
 * Start the class autoloader and namespacing
 */

if( ! class_exists('Psr4AutoloaderClass') ) {
	include_once( 'src/class_autoloader.php' );
}
$loader = new  \cscs_ns\Psr4AutoloaderClass;
// register the  namespaces
$loader->addNamespace( 'cscs_ns', dirname( __FILE__ ) . '/' );
$loader->addNamespace( 'cscs_ns/src', dirname( __FILE__ ) . '/src' );
// register the autoloader
$loader->register();

/**
 *  Setup the Publication CPT
 */
	$pubLabels = array(
		'all_items' => 'All Publications',
		'not_found' => 'No publications found'
	);

	$pubArgs        = array(
		'rewrite'            => array('slug' => 'publications'),
		'capability_type'     => array('Publication', 'Publications'),
		'map_meta_cap'        => true,
		'show_ui'            => true,
		'show_in_admin_bar'  => true,
		'has_archive'        => true,
		'can_export '        => true,
		'public'             => true,
		'hierarchical'       => false,
		'publicly_queryable' => false,
		'menu_position'      => 10,

	);
	$publication = new \cscs_ns\src\cscs_cpt( 'Publication', $pubArgs, $pubLabels );


/**
 *  Setup the Project CPT
 */

$projLabels = array(
	'name'                  => _x( 'Projects', 'Post Type General Name', 'text_domain' ),
	'all_items' => 'All Projects',
	'not_found' => 'No projects found',
	'menu_name'             => 'Research Projects'
);

$projArgs         = array(
	'rewrite' => array('slug' => 'projects','with_front' => false),
	'capability_type'     => array('Project', 'Projects'),
	'map_meta_cap'        => true,
		// if this is changed, you will to need to  "Save Changes" in  Settings > Permalinks
	'show_ui'            => true,
	'show_in_admin_bar'  => true,
	'has_archive'        => true,
	'can_export '        => true,
	'public'             => true,
	'hierarchical'       => false,
	'publicly_queryable' => true,
	'menu_position'      => 10

);
$projects = new \cscs_ns\src\cscs_cpt( 'project', $projArgs, $projLabels );

/*
 * Set up the staff CPT
 */

$staffLabels = array(
	'name'      => _x( 'staff', 'Post Type General Name', 'text_domain' ),
	'all_items' => 'All Staff',
	'not_found' => 'No staff found',
	'menu_name' => 'Staff',
);
$staffArgs         = array(
	'rewrite'    => array('slug' => 'staff','with_front' => false),
	// if this is changed, you will to need to  "Save Changes" in  Settings > Permalinks
	'capability_type'     => array('post', 'posts'),
	'map_meta_cap'        => true,
	'show_ui'            => true,
	'show_in_admin_bar'  => true,
	'has_archive'        => true,
	'can_export '        => true,
	'public'             => true,
	'hierarchical'       => false,
	'publicly_queryable' => true,
	'menu_position'      => 10,

);
$staff = new \cscs_ns\src\cscs_cpt( 'staff', $staffArgs, $staffLabels );


/*
 * Set up the Tests CPT
 */

$testsLabels = array(
	'all_items' => 'All Tests',
	'not_found' => 'No test found'
);

$testsArgs        = array(
	'rewrite'            => array('slug' => 'tests', 'with_front' => false),
	'capability_type'     => array('Test', 'Tests'),
	'map_meta_cap'        => true,
	'show_ui'            => true,
	'show_in_admin_bar'  => true,
	'has_archive'        => true,
	'can_export '        => true,
	'public'             => true,
	'hierarchical'       => false,
	'publicly_queryable' => true,
	'menu_position'      => 11,

);
$test = new \cscs_ns\src\cscs_cpt( 'Test', $testsArgs, $testsLabels );

/*
 * Setup project taxonomies
 */

$project_volunteer_type_args = array('meta_box_cb'=> false);
$project_volunteer_type = new \cscs_ns\src\cscs_taxonomies('Volunteer Type','',$project_volunteer_type_args,array('project'));

/*
 * Setup publication taxonomies
 */
$publishers_args = array('meta_box_cb'=> false);
$publishers = new \cscs_ns\src\cscs_taxonomies('Publisher','',$publishers_args,array('publication'));
// Create shared taxonomies
$keywords_args = array(	'hierarchical'=> true);
$keywords = new \cscs_ns\src\cscs_taxonomies('Keyword','',$keywords_args,array('publication','post','staff'));
// Shared with posts (to allow news posts to be linked)
$journal_args= array('meta_box_cb'=> false);
$journal= new \cscs_ns\src\cscs_taxonomies('Publication Journal','',$journal_args,array('post','publication'));

$translations_args = array('meta_box_cb'=> false);
$translations = new \cscs_ns\src\cscs_taxonomies('Translation','',$translations_args,array('test'));

$affiliation_args = array(
	'meta_box_cb'=> false,
	'hierarchical'               => false,
	'public'                     => true,
	'show_ui'                    => false,
	'show_admin_column'          => true,
	'show_in_nav_menus'          => true,
	'show_tagcloud'              => false,


	);
$affiliation_labels = array(
	'name' => _x('Affiliations', 'taxonomy general name', 'staff_cpt'),
	'singular_name' => _x('Affiliation', 'taxonomy singular name', 'staff_cpt'),
	'search_items' => __('Search Affiliations', 'staff_cpt'),
	'all_items' => __('All Affiliations', 'staff_cpt'),
	'edit_item' => __('Edit Affiliation', 'staff_cpt'),
	'update_item' => __('Update Affiliation', 'staff_cpt'),
	'add_new_item' => __('Add Affiliation', 'staff_cpt'),
	'new_item_name' => __('New Affiliation', 'staff_cpt'),
	'menu_name' => __('Affiliations', 'staff_cpt'),
);
$affiliation = new \cscs_ns\src\cscs_taxonomies('Staff Affiliation','',$journal_args,array('post','staff'));


/*
 * Set up the slider CPT
 */

$sliderLabels = array(
	'all_items' => 'All slides',
	'not_found' => 'No sliders found'
);

$sliderArgs        = array(
	'rewrite'            => array('slug' => 'arc-slider', 'with_front' => false),
	'capability_type'     => array('Slider', 'Sliders'),
	'map_meta_cap'        => true,
	'show_ui'            => true,
	'show_in_admin_bar'  => true,
	'has_archive'        => false,
	'can_export '        => false,
	'public'             => true,
	'hierarchical'       => false,
	'publicly_queryable' => false,
	'menu_position'      => 12,
    'menu_icon'          => 'dashicons-images-alt2'

);
$slider = new \cscs_ns\src\cscs_cpt( 'Slider', $sliderArgs, $sliderLabels );

// remove some stuff from slideshows - we just want image and title for now...

add_action( 'init', 'arc_cleanup_supports_init' );
function arc_cleanup_supports_init() {
	remove_post_type_support( 'slider', 'editor' );
    remove_post_type_support( 'slider', 'excerpt' );
}

/*
 * Add some new image sizes
 */
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'homepage-thumb', 300, 200, true ); //(cropped)
    add_image_size('project-list',750,414,true);
	add_image_size('staff-portrait',150,165,true);
}

/*
 * Include the general file for other functions
 * Implement the Staff shortcode
 */

define( 'plugin_DIR', dirname( __FILE__ ) );

include_once( plugin_DIR . '/src/general.php' );
// n.b. taxonomies need to be defined before the associated CPT for custom taxonomies slug to work :-)
include_once( plugin_DIR . '/src/staff_taxonomies.php' );
include_once( plugin_DIR . '/src/programme_taxonomies.php' );
// include_once( plugin_DIR . '/src/staff-sidebar.php' );
include_once( plugin_DIR . '/src/shortcodes.php' );


/**
 * Set up ACF field definitions in JSON
 * TODO: refactor as a helper class
 */
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
	// update path
	$path = plugin_dir_path( __FILE__ ) . '/acf-json';
	// return
	return $path;
}
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
	// remove original path (optional)
	unset( $paths[0] );
	// append path
	$paths[] = plugin_dir_path( __FILE__ ) . '/acf-json';

	// return
	return $paths;
}

/*
 * Add in Site Options admin panel
 */
new \cscs_ns\src\site_options();

/**
 * Override page title functionality. This is set BEFORE the site-inner class so that it can go full
 * width.
 * Inspiration: https://benweiser.com/add-a-featured-post-image-with-title-in-genesis/
 *
 * dt216: add: - archive intro text below header
 */
if (!has_post_thumbnail()) {
	add_action( 'genesis_after_header', 'bw_featured_image_title' );
}
function bw_featured_image_title($title) {
	
    // Checkpoint: Abort if we're on the home/front-page, or on a post/page without a featured image
	$cpt_archive_id = get_post_type( get_the_ID() );
	$cpt_archive_page = 'cpt_'.$cpt_archive_id;
    $image = get_field( 'cpt_archive_image', $cpt_archive_page );
    if (is_home() || is_front_page()||(is_single() && !has_post_thumbnail()) || (is_archive() &&!$image) || (!is_archive() && !has_post_thumbnail())){return;}
   	if (is_archive()){
            // Use archive options page plugin and ACF image uploader field
            // Retrieve archive image if one has been uploaded
            $image_desktop = $image['url'];
            $image_desktop_size = $image['sizes']['large-height'];
	        $image_mobile = $image['url'];
	        $image_mobile_size = $image['sizes']['medium-height'];
	        // remove the default CPT archive title and description (can only remove both at once)
	}

	// Stuff for single posts
    elseif (!is_archive()  ) {
	    $image_desktop = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' )[0];
	    $image_mobile  = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'medium' )[0];
	    $image_desktop_size = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' )[2];
	    $image_mobile_size  = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'medium' )[2];
	    //remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
	    //add_action('bw_featured_title', 'genesis_do_post_title');
	    //remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
	    //remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

    }
    $featured_class = 'bw-featured-title';

	// Insert the Genesis opening structural markup for the entry header in the action hook below.
	add_action( 'before_bw_featured_title', 'genesis_entry_header_markup_open', 5 );
	add_action( 'after_bw_featured_title', 'genesis_entry_header_markup_close', 15 );


// Display the header, with the hook to pull in the image from above code
 ?>

<div class='<?php echo $featured_class; ?> header-image'>
    <?php
       do_action('before_bw_featured_title',$title);
       do_action('bw_featured_title',$title);
       do_action('after_bw_featured_title',$title);
	?>
<div class="image-ref">
    <?php echo wp_get_attachment_caption( get_post_thumbnail_id( $post_id ) ); ?>
</div>
</div>
<style>
	<?php echo ".$featured_class "; ?> {
		background-image:url( <?php echo $image_mobile; ?>);
		max-height:<?php echo $image_mobile_size . "px"; ?>;
		height:<?php echo $image_mobile_size . "px"; ?>;

	}
	.site-inner {margin-top: 50px;}
    .bw-featured-title{min-height: 300px;}
    .entry-header::after{padding-bottom:0px;}

	@media only screen and (min-width : 992px) {
        <?php echo ".$featured_class "; ?> {
        	background-image:url(<?php echo $image_desktop;?>);
            max-height:<?php echo $image_desktop_size . "px"; ?>;
			height:300px;
			
        }
}
</style>

<?php

}

/* Offload PDF to S3
 * Depends on the WP Media Offloader Lite plugin (https://wordpress.org/plugins/amazon-s3-and-cloudfront/)
 * Key config. in wp-config, bucket and misc. other  in plugin settings
 */

new \cscs_ns\src\WP_Offload_S3_File_Type_Filter();

// Disables WordPress from generating thumbnail images when a PDF is uploadedby offloader plugin
function cscs_disable_pdf_thumbnails() {
	$fallbacksizes = array();
	return $fallbacksizes;
}
add_filter('fallback_intermediate_image_sizes', 'cscs_disable_pdf_thumbnails');

/*
 * Call the Project CPT widget
 */
// Register the widget
new \cscs_ns\src\CSCS_CPT_Widget();

function cscs_cpt_register_custom_widget() {
	register_widget( 'cscs_ns\src\CSCS_CPT_Widget' );
	register_widget( 'cscs_ns\src\CSCS_News_Widget' );
}
add_action( 'widgets_init', 'cscs_cpt_register_custom_widget' );

// quick fix for the 3 character search limit:

function cscs_searchwp_minimum_word_length() {
  // index and search for words with at least two characters
  return 2;
}

add_filter( 'searchwp_minimum_word_length', 'cscs_searchwp_minimum_word_length' );

function cscs_searchwp_search_partials($length) {
	return 2;
}

add_filter( 'searchwp_like_min_length', 'cscs_searchwp_search_partials' );

// order publication results by date:
function cscs_searchwp_return_orderby_date( $order_by_date, $engine ) {
	// for the publications engine (only!) we want to return search results by date
	if ( 'publications_search' == $engine ) {
		$order_by_date = true;
	}
	return $order_by_date;
}
add_filter( 'searchwp_return_orderby_date', 'cscs_searchwp_return_orderby_date', 10, 2 );

// add AJAX actions:

add_action( 'wp_ajax_cscs_slugify_ajax', 'cscs_slugify_ajax' );
add_action( 'wp_ajax_nopriv_cscs_slugify_ajax', 'cscs_slugify_ajax' );

function cscs_slugify_ajax() {

    $output = array();

    if( '' === $_REQUEST['lang'] ) :
        $output['error'] = 'No lang set';
    else :
        // normally do something link:
        // $language_raw = filter_var( $_REQUEST['lang'], FILTER_SANITIZE_STRING );
        // before doing something with it. In this case, however, the whole purpose
        // of what we're doing is sanitization, so it's probably(!) safe to go ahead:
        $lang_trim = preg_replace( '/\ \([0-9]+\)/', '', $_REQUEST['lang'] );
        $language_sanitized = sanitize_title( $lang_trim );
        $output['error'] = FALSE;
        $output['slug'] = $language_sanitized;
    endif;

    wp_send_json( $output );
    //wp_send_json handles die() for us.
}


function cscs_add_mimes( $mimes ) {
    $mimes['png'] = 'image/png';
    $mimes['jpg'] = 'image/jpeg';
    return $mimes;
}

add_filter( 'as3cf_allowed_mime_types', 'cscs_add_mimes', 10, 1 );

// add_action( 'genesis_header', 'cscs_do_header' );
// function cscs_do_header() {
// 	global $wp_registered_sidebars;
// 	echo "<div class='header-site-title'><h1>" . get_bloginfo( 'title' ) . "</h1></div>";

// }



add_filter( 'theme_page_templates', 'cscs_page_templates' );
function cscs_page_templates( $templates ) {
	$templates['template-past-projects.php'] = 'Past projects';
	return $templates;
}

/**
 * Check if template exists in theme or fall back to plugin
 *
 * @param string/array $template
 * @return string
 */

define( 'ARCWP_DIR', plugin_dir_path( __FILE__ ) );
add_filter( 'template_include', 'cscs_product_page_template', 99 );

function cscs_product_page_template( $template ) {

	if ( is_singular( ) ) :
	$page_template = get_page_template_slug( get_queried_object_id() );

	if ( ! $page_template ) :
		return $template;
		endif;
	if ( file_exists( STYLESHEETPATH . '/' . $page_template ) ) :
		$located = STYLESHEETPATH . '/' . $page_template;
		return $located;
		elseif ( file_exists( TEMPLATEPATH . '/' . $page_template ) ) :
			$located = TEMPLATEPATH . '/' . $page_template;
			return $located;
		elseif ( file_exists( ARCWP_DIR . '/templates/' . $page_template ) ) :
			$located = ARCWP_DIR . '/templates/' . $page_template;
			return $located;
		endif;
	else :
		return $template;
	endif;
}


remove_action( 'genesis_header', 'genesis_do_header' );
/**
 * Echo the default header, including the #title-area div, along with #title and #description, as well as the .widget-area.
 *
 * Does the `genesis_site_title`, `genesis_site_description` and `genesis_header_right` actions.
 *
 * @since 1.0.2
 *
 * @global $wp_registered_sidebars Holds all of the registered sidebars.
 */

add_action( 'genesis_header', 'arc_do_header' );

function cscs_append_search( $menu ) {
	$return = $menu . "<div class='twitter-header-mobile'><a href='https://twitter.com/arc_cambridge' title='ARC Twitter'><i class='fa fa-twitter'></i></a></div>";
	$return .= "<div class='mobile-search'>" . cscs_return_search_form() . "</div>";
	return $return;
}

add_filter( 'wp_nav_menu', 'cscs_append_search' );

remove_action( 'arc_do_header', 'genesis_site_title' );

function arc_do_header() {

	//global $wp_registered_sidebars;
	echo "<div class='header-site-title'><h1><a href='" . get_site_url() . "'>" . get_bloginfo( 'title' ) . "</a></h1></div>";
	// genesis_markup(
	// 	[
	// 		'open'    => '<div %s>',
	// 		'context' => 'title-area',
	// 	]
	// );
	
	// /**
	//  * Fires inside the title area, before the site description hook.
	//  *
	//  * @since 2.6.0
	//  */
	// // do_action( 'genesis_site_title' );

	// /**
	//  * Fires inside the title area, after the site title hook.
	//  *
	//  * @since 1.0.0
	//  */
	// // do_action( 'genesis_site_description' );

	// genesis_markup(
	// 	[
	// 		'close'   => '</div>',
	// 		'context' => 'title-area',
	// 	]
	// );
	// echo "<div class='header-site-title-right'>";
	
	
	// echo "<h1>" . get_bloginfo( 'title' ) . "</h1>";
	
	
	// echo "</div>";
	echo "<div class='row search-row'>";
	echo "<div class='twitter-header'><a href='https://twitter.com/arc_cambridge' title='ARC Twitter'><i class='fa fa-twitter'></i></a></div>";
	//cscs_do_search_form();
	echo "</div>";
	if ( has_action( 'genesis_header_right' ) || ( isset( $wp_registered_sidebars['header-right'] ) && is_active_sidebar( 'header-right' ) ) ) {

		genesis_markup(
			[
				'open'    => '<div %s>',
				'context' => 'header-widget-area',
			]
		);

		

			/**
			 * Fires inside the header widget area wrapping markup, before the Header Right widget area.
			 *
			 * @since 1.5.0
			 */
			//do_action( 'genesis_header_right' );
			add_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
			$menu = genesis_header_menu_wrap();

			$menu = $menu . cscs_do_header();
			//add_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );
			add_filter( 'wp_nav_menu', $menu );
			dynamic_sidebar( 'header-right' );
			
			remove_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
			remove_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );

		genesis_markup(
			[
				'close'   => '</div>',
				'context' => 'header-widget-area',
			]
		);

	}

}

function cscs_return_search_form() {
	$output = '';
	$url = get_bloginfo('url');
	$output .= "<form method='get' action='$url/search-results'><input type='search' name='fwp_site_search' id='fwp_site_search' placeholder='Search' />
				<input type='submit' value='Go' />
				</form>";

	return $output;
}

function cscs_do_search_form() {
	echo cscs_return_search_form();
}

function cscs_do_pre_header() {
	?>
		<div class="cscs_preheader">
			<div class="wrap">
				<div class="logoblock">
					<h1>University of Cambridge</h1>
				</div>
				<div class="searchblock">
					<?php cscs_do_search_form(); ?>
				</div>
			</div>
		</div>

	<?php
}

add_action( 'genesis_before', 'cscs_do_pre_header' );

add_filter( 'facetwp_is_main_query', function( $is_main_query, $query ) {
    if ( $query->is_archive() && $query->is_main_query() ) {
        $is_main_query = false;
    }
    return $is_main_query;
}, 10, 2 );
